// console.log(Routing.generate('delete_item_from_cart',{id: 3}));



function deleteArticle(id, total_product_price) {
    $("#qte"+id).val(0);
    $.ajax({
        url: Routing.generate('delete_item_from_cart', {id: id}), success: function (result) {
            console.log(result);
            /*$(".product-"+id).remove();

             total_price = $(".total_price_cart").html() - total_product_price;

             if(total_price<0 || $(".nb-article").html() - 1 == 0) total_price = 0;
             $(".total_price_cart").html(total_price.toFixed(2));
             $(".total_price_cart_empty").html(total_price.toFixed(2));

             $(".nb-article").html($(".nb-article").html() - 1);*/
            $(".product-" + id).remove();
            $(".my_nav_bar").html(result);
            $(".total_price_cart_empty").html($(".total_price_cart").html());
            $("#total-price").html($(".total_price_cart_empty").html())
        }
    });
}
function verifPanier() {
    $.ajax({
        url: Routing.generate('verification_panier'), success: function (result) {
            console.log(result);
            $("#qte"+id).val(0);
        }
    });
}

function deleteArticlePagePanier(id, total_product_price,user) {
    $.ajax({
        url: Routing.generate('delete_item_from_cart', {id: id}), success: function (result) {
            console.log(result);
            /*$(".product-"+id).remove();

             total_price = $(".total_price_cart").html() - total_product_price;

             if(total_price<0 || $(".nb-article").html() - 1 == 0) total_price = 0;
             $(".total_price_cart").html(total_price.toFixed(2));
             $(".total_price_cart_empty").html(total_price.toFixed(2));

             $(".nb-article").html($(".nb-article").html() - 1);*/
            $(".product-" + id).remove();
            $(".my_nav_bar").html(result);
            $(".total_price_cart_empty").html($(".total_price_cart").html());
            $("#total-price").html($(".total_price_cart_empty").html())

            if($(".total_price_cart_empty").html()!="0.000" && user=='true'){
                $('#btn-livrer').addClass("btn-primary");
                $('#btn-livrer').attr("href",Routing.generate('livraison'));
                $('#btn-livrer').html("Livrer <i class='fa fa-arrow-right'></i>");
            }else if($(".total_price_cart_empty").html()=="0.000" && user=='true'){
                $('#btn-livrer').attr("href","#");
                $('#btn-livrer').addClass("btn-danger");
                $('#btn-livrer').html("Panier vide <i class='fa fa-exclamation-circle'></i>");
            }else{
                $('#btn-livrer').attr("href",Routing.generate('fos_user_security_login'));
                $('#btn-livrer').addClass("btn-danger");
                $('#btn-livrer').html("Connecter pour commander&nbsp; <i class='fa fa-exclamation-circle'></i>");
            }
        }
    });
}


function addArticle(id, quantity) {
    var qte = parseInt(  $("#qte"+id).val());
    //var quantity = document.getElementById("qte"+id).value;
    console.log(qte+ 1);
    console.log(quantity);
    if(qte+ 1  > quantity  ){
        $("document").ready(function () {
            $('#modal-panier').modal({
                backdrop: 'static',
                keyboard: false
            });
            //  $('select').select2();
        })
    }
    else {
        $("#qte"+id).val(qte + 1);
        var $this = $("#addArticle"+id);
    $.ajax({
        url: Routing.generate('add_quantity_to_cart', {id: id}), success: function (result) {
            $(".my_nav_bar").html(result);
            $(".total_price_cart_empty").html($(".total_price_cart").html());
            $this.button('reset');
        }
    });
}
}
function deleteArticleQuantity(id, product_price) {

    var quantity = parseInt(  $("#qte"+id).val());
    if (quantity > 0) {
        $("#qte"+id).val(quantity - 1);
        var $this = $("#deleteArticle"+id);



        $.ajax({
            url: Routing.generate('delete_quantity_to_cart', {id: id}), success: function (result) {
                $(".my_nav_bar").html(result);
                $(".total_price_cart_empty").html($(".total_price_cart").html());
                $this.button('reset');
            }
        });
    }

}

function addArticleQuantite(id, product_price,quantite) {
    for (var i=0;i<quantite;i++){
        addArticle(id,product_price);
    }
}


function goToURL(url) {
    location.href = url;
    console.log(url);
}
$(document).ready(function () {
    $(".total_price_cart_empty").html($(".total_price_cart").html());

    last_val = 0;
    $(".code").keyup(function () {
        console.log($(this).val());
        if ($(this).val().length === 4 && last_val != $(this).val()) {
            last_val = $(this).val();
            $(".delegation option").remove();
            $.ajax({
                type: 'get',
                url: Routing.generate('delegations', {cp: $(this).val()}),
                beforeSend: function () {
                    if ($(".loading").length == 0) {
                        $("form .delegation").parent().append('<i class="loading fa fa-spinner fa-spin" style="font-size: 22px"></i>');
                    }
                    $(".delegation option").remove();
                },
                success: function (data) {
                    if (data.ville) {
                        $.each(data.ville, function (index, value) {
                            $(".delegation").append($('<option>', {value: index, text: value}));
                        });
                    }
                    $(".loading").remove();
                }
            });
        } else {
            $(".delegation").val('');
        }
    });
});