<?php

namespace BackendBundle\Controller\GerantStation;


use BackendBundle\Form\HoraireType;
use Proxies\__CG__\WebBundle\Entity\Station;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/horaire_gerant")
 */
class HoraireController extends Controller
{
/*
    public function index(Request $request): Response
    {

        $em = $this->getDoctrine()->getManager();
        $station=$em->getRepository('WebBundle:Station')->find($this->getUser()->getStation()->getId());
        $form = $this->createForm(HoraireType::class, $station);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->merge($station);
            $em->flush();
            return $this->redirectToRoute('horaire_gerant_index');
        }


        return $this->render('@Backend/gerant/horaire/index.html.twig', [
            'station' => $station,
            'form' => $form->createView(),
        ]);

    }
*/


    /**
     * @Route("/", name="horaire_gerant_index", methods={"GET","POST"})

     */
    public function index(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $station=$em->getRepository('WebBundle:Station')->find($this->getUser()->getStation()->getId());

        $horaires=new Station();

        if ($request->isMethod('POST')) {
           $listHoraire=$request->get('listHoraires');
           $is_open=$request->get('isOpen','0');
           if($listHoraire == null){
               return $this->render('@Backend/gerant/horaire/horaire.html.twig', [
                   'station' => $station,
                   'horaires'=>$horaires::$horaire_values,
                   'error' =>'Vous devez choisir au moins un horaire de retrait'
               ]);
           }
            foreach ($listHoraire as  $value){
                if(!in_array($value,$station->getHoraires())){
                   $station->addHoraire($value);
                }
            }

            foreach ($station->getHoraires()  as  $value){
                if(!in_array($value,$listHoraire)){
                    $station->removeHoraire($value);
                }
            }
            $station->setIsOpen($is_open);
           $em->merge($station);
           $em->flush();
        }


        return $this->render('@Backend/gerant/horaire/horaire.html.twig', [
            'station' => $station,
            'horaires'=>$horaires::$horaire_values
        ]);

    }

    /**
     * @Route("/Liste_des_clients_gerant", name="ExportClient_gerant" ,defaults={"_format"="xls","_filename"="Liste_des_clients"}, requirements={"_format"="csv|xls|xlsx"})
     * @Template("@Backend/excel/ClientExcel.xlsx.twig")
     */
    public function ExportModeleAction($_filename, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $email=$request->get('email',"");
        $gerants =$em->getRepository('WebBundle:Utilisateurs')->getClientByStation($this->getUser()->getStation(),$email);
        return $this->render('@Backend/excel/ClientExcel.xlsx.twig', array(
            'Modeles' => $gerants,
        ));
    }
}
