<?php

namespace BackendBundle\Controller\GestionnairePays;


use BackendBundle\Form\CategorieType;
use BackendBundle\Form\SousCategorieType;
use BackendBundle\Form\StationType;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Pdf;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Dompdf\Dompdf;
use Dompdf\Options;
use WebBundle\Entity\Categories;
use WebBundle\Entity\SousCategories;
use WebBundle\Entity\Station;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/sous_categorie")
 */
class SousCategorieController extends Controller
{

    /**
     *
     * @Route("/", name="sous_categorie_index", defaults={"page": 1})
     * @Route("/page/{page}", requirements={"page": "[1-9]\d*"}, name="sous_categorie_index_paginated")
     * @Method("GET")
     */
    public function index(Request $request,$page)
    {

        $em = $this->getDoctrine()->getManager();
        $search=$request->get('search',"");
        $categorie_id=$request->get('categorie_id',"");

        $souscategories =$em->getRepository('WebBundle:SousCategories')->getSousCategorieByPays($this->getUser()->getPays(),$search,$categorie_id);
        $categories =$em->getRepository('WebBundle:Categories')->getCategorieByPays($this->getUser()->getPays(),"");
        $paginator = $this->get('knp_paginator');
        $souscategories_paginator = $paginator->paginate(
            $souscategories, $page, 10
        //Produits::NUM_ITEMS
        );
        $souscategories_paginator->setUsedRoute('sous_categorie_index_paginated');
        return $this->render('@Backend/gestionnaire/souscategorie/index.html.twig', [
            'souscategories' => $souscategories_paginator,
            'categories' => $categories,
            'search' => $search,
            'categorie_id'=>$categorie_id,
            'nbsouscategories' => $souscategories,

        ]);
    }

    /**
     * @Route("/new", name="sous_categorie_new", methods={"GET","POST"})
     */
    public function newAction(Request $request)
    {
        $formOptions = array('pays' => $this->getUser()->getPays());
        $souscategorie = new SousCategories();
        $form = $this->createForm(SousCategorieType::class, $souscategorie,$formOptions);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $image=  $form->get('imagefile')->getData();
            if ($image) {
                $originalFilename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$image->guessExtension();

                try {
                    $image->move(
                        $this->getParameter('image_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                }
                $souscategorie->setImage($newFilename);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $souscategorie->setPays($this->getUser()->getPays());
            $entityManager->persist($souscategorie);
            $entityManager->flush();

            return $this->redirectToRoute('sous_categorie_index');
        }

        return $this->render('@Backend/gestionnaire/souscategorie/new.html.twig', [
            'souscategorie' => $souscategorie,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}/edit", name="sous_categorie_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SousCategories $sousCategorie)
    {
        $formOptions = array('pays' => $this->getUser()->getPays());
        $form = $this->createForm(SousCategorieType::class, $sousCategorie,$formOptions);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $image=  $form->get('imagefile')->getData();
            if ($image) {
                $originalFilename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$image->guessExtension();

                try {
                    $image->move(
                        $this->getParameter('image_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                }
                $sousCategorie->setImage($newFilename);
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sous_categorie_index');
        }

        return $this->render('@Backend/gestionnaire/souscategorie/edit.html.twig', [
            'souscategorie' => $sousCategorie,
            'form' => $form->createView(),
        ]);
    }


    /**
     * disable station.
     *
     * @Route("/{id}/disable", name="sous_categorie_disable", methods={"GET"})
     */
    public function disableAction(SousCategories $souscategorie)
    {
        $em = $this->getDoctrine()->getManager();
        $onesouscategorie =$em->getRepository('WebBundle:SousCategories')->find($souscategorie->getId());
        $onesouscategorie->setIsActive(false);
        $em->merge($onesouscategorie);
        $em->flush();
        return $this->redirectToRoute('sous_categorie_index');
    }
    /**
     * @Route("/Liste_des_sous_categories", name="ExportSousCategorie" ,defaults={"_format"="xls","_filename"="Liste_des_sous_categories"}, requirements={"_format"="csv|xls|xlsx"})
     * @Template("@Backend/excel/SousCategorieExcel.xlsx.twig")
     */
    public function ExportModeleAction($_filename, Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        parse_str(parse_url($this->getRequest()->headers->get('referer'), PHP_URL_QUERY), $output);

        if(array_key_exists('search',$output)) $search=$output['search']; else $search='';
        if(array_key_exists('categorie_id',$output)) $categorie_id=$output['categorie_id']; else $categorie_id='';

        $souscategories =$em->getRepository('WebBundle:SousCategories')->getSousCategorieByPays($this->getUser()->getPays(),$search,$categorie_id);
        return $this->render('@Backend/excel/SousCategorieExcel.xlsx.twig', array(
            'Modeles' => $souscategories,
        ));
    }

    }
