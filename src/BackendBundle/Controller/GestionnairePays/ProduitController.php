<?php

namespace BackendBundle\Controller\GestionnairePays;


use BackendBundle\Form\ProduitType;
use BackendBundle\Form\StationType;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Pdf;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Dompdf\Dompdf;
use Dompdf\Options;
use WebBundle\Entity\Produits;
use WebBundle\Entity\Station;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
/**
 * @Route("/produit")
 */
class ProduitController extends Controller
{
    /**
     *
     * @Route("/", name="produit_index", defaults={"page": 1})
     * @Route("/page/{page}", requirements={"page": "[1-9]\d*"}, name="produit_index_paginated")
     * @Method("GET")
     */
    public function index(Request $request,$page)
    {

        $em = $this->getDoctrine()->getManager();
        $search=$request->get('search',"");
        $categorie=$request->get('categorie',"");
        $souscategorie=$request->get('souscategorie',"");
        $station=$request->get('station',"");

        $categories =$em->getRepository('WebBundle:Categories')->getCategorieByPays($this->getUser()->getPays(),"");
        $souscategories =$em->getRepository('WebBundle:SousCategories')->getSousCategorieByPays($this->getUser()->getPays(),"","");
        $stations =$em->getRepository('WebBundle:Station')->getStationByPays($this->getUser()->getPays(),"");
        $produits =$em->getRepository('WebBundle:Produits')->getProduitByPays($this->getUser()->getPays(),$search,$categorie,$souscategorie,$station);
        $paginator = $this->get('knp_paginator');
        $produits_paginator = $paginator->paginate(
            $produits, $page, 10
        //Produits::NUM_ITEMS
        );
        $produits_paginator->setUsedRoute('produit_index_paginated');

        return $this->render('@Backend/gestionnaire/produit/index.html.twig', [
            'produits' => $produits_paginator,
            'search' => $search,
            'categorie' => $categorie,
            'souscategorie' => $souscategorie,
            'station' => $station,
            'categories' => $categories,
            'souscategories' => $souscategories,
            'stations' => $stations,
            'nbproduits' => $produits,

        ]);
    }
    /**
     * @Route("/get_sous_categorie_from_categorie", name="get_sous_categorie_from_categorie", methods={"GET"})
     */
    public function getSousCategorieFromCategorie(Request $request)
    {
        // Get Entity manager and repository
        $em = $this->getDoctrine()->getManager();
        $sousCaregorieRepository = $em->getRepository("WebBundle:SousCategories");

        // Search the neighborhoods that belongs to the city with the given id as GET parameter "cityid"
        $sousCategories = $sousCaregorieRepository->createQueryBuilder("sc")
            ->where("sc.categorie = :categorieid")
            ->setParameter("categorieid", $request->query->get("categorieid"))
            ->getQuery()
            ->getResult();

        // Serialize into an array the data that we need, in this case only name and id
        // Note: you can use a serializer as well, for explanation purposes, we'll do it manually
        $responseArray = array();
        foreach($sousCategories as $souscategorie){
            $responseArray[] = array(
                "id" => $souscategorie->getId(),
                "nom" => $souscategorie->getNom()
            );
        }

        return new JsonResponse($responseArray);

    }
    /**
     * @Route("/new", name="produit_new", methods={"GET","POST"})
     */
    public function newAction(Request $request)
    {
        $produit = new Produits();
        $formOptions = array('pays' => $this->getUser()->getPays());

        $form = $this->createForm(ProduitType::class, $produit,$formOptions);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
          /*  $image=  $form->get('imagefile')->getData();
            if ($image) {
                $originalFilename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$image->guessExtension();

                try {
                    $image->move(
                        $this->getParameter('image_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                }
                $produit->setImage($newFilename);
            }
            */
            $entityManager = $this->getDoctrine()->getManager();
            $produit->setPays($this->getUser()->getPays());
            $produit->setCode(uniqid());
            $entityManager->persist($produit);
            $entityManager->flush();

            return $this->redirectToRoute('produit_index');
        }

        return $this->render('@Backend/gestionnaire/produit/new.html.twig', [
            'produit' => $produit,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}/edit", name="produit_edit", methods={"GET","POST"})
     */
    public function editAction(Request $request, Produits $produit)
    {
        $formOptions = array('pays' => $this->getUser()->getPays());

        $form = $this->createForm(ProduitType::class, $produit,$formOptions);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $image=  $form->get('imagefile')->getData();
            if ($image) {
                $originalFilename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$image->guessExtension();

                try {
                    $image->move(
                        $this->getParameter('image_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                }
                $produit->setImage($newFilename);
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('produit_index');
        }

        return $this->render('@Backend/gestionnaire/produit/edit.html.twig', [
            'produit' => $produit,
            'form' => $form->createView(),
        ]);
    }

    /**
     * disable station.
     *
     * @Route("/{id}/disable", name="produit_disable", methods={"GET"})
     */
    public function disableAction(Produits $produit)
    {
        $em = $this->getDoctrine()->getManager();
        $onestation =$em->getRepository('WebBundle:Produits')->find($produit->getId());
        $onestation->setIsActive(false);
        $em->persist($onestation);
        $em->flush();
        return $this->redirectToRoute('produit_index');
    }

    /**
     * @Route("/Liste_des_produits", name="ExportProduit" ,defaults={"_format"="xls","_filename"="Liste_des_stations"}, requirements={"_format"="csv|xls|xlsx"})
     * @Template("@Backend/excel/StationExcel.xlsx.twig")
     */
    public function ExportModeleAction($_filename, Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        parse_str(parse_url($this->getRequest()->headers->get('referer'), PHP_URL_QUERY), $output);

        if(array_key_exists('search',$output)) $search=$output['search']; else $search='';
        if(array_key_exists('categorie',$output)) $categorie=$output['categorie']; else $categorie='';
        if(array_key_exists('souscategorie',$output)) $souscategorie=$output['souscategorie']; else $souscategorie='';
        if(array_key_exists('station',$output)) $station=$output['station']; else $station='';

        $produits =$em->getRepository('WebBundle:Produits')->getProduitByPays($this->getUser()->getPays(),$search,$categorie,$souscategorie,$station);
        return $this->render('@Backend/excel/ProduitExcel.xlsx.twig', array(
            'Modeles' => $produits,
        ));
    }
}
