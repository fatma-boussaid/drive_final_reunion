<?php

namespace BackendBundle\Controller\GestionnairePays;


use BackendBundle\Form\CategorieType;
use BackendBundle\Form\StationType;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Pdf;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Dompdf\Dompdf;
use Dompdf\Options;
use WebBundle\Entity\Categories;
use WebBundle\Entity\Station;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
/**
 * @Route("/categorie")
 */
class CategorieController extends Controller
{

    /**
     *
     * @Route("/", name="categorie_index", defaults={"page": 1})
     * @Route("/page/{page}", requirements={"page": "[1-9]\d*"}, name="categorie_index_paginated")
     * @Method("GET")
     */
    public function index(Request $request,$page)
    {

        $em = $this->getDoctrine()->getManager();
        $search=$request->get('search',"");


        $categories =$em->getRepository('WebBundle:Categories')->getCategorieByPays($this->getUser()->getPays(),$search);
        $paginator = $this->get('knp_paginator');
        $categories_paginator = $paginator->paginate(
            $categories, $page, 10
        //Produits::NUM_ITEMS
        );
        $categories_paginator->setUsedRoute('categorie_index_paginated');
        return $this->render('@Backend/gestionnaire/categorie/index.html.twig', [
            'categories' => $categories_paginator,
            'search' => $search,
            'nbcategories' => $categories,

        ]);
    }

    /**
     * @Route("/new", name="categorie_new", methods={"GET","POST"})
     */
    public function newAction(Request $request)
    {
        $categorie = new Categories();
        $form = $this->createForm(CategorieType::class, $categorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $categorie->setPays($this->getUser()->getPays());
            $entityManager->persist($categorie);
            $entityManager->flush();

            return $this->redirectToRoute('categorie_index');
        }

        return $this->render('@Backend/gestionnaire/categorie/new.html.twig', [
            'categorie' => $categorie,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}/edit", name="categorie_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Categories $categorie)
    {
        $form = $this->createForm(CategorieType::class, $categorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('categorie_index');
        }

        return $this->render('@Backend/gestionnaire/categorie/edit.html.twig', [
            'categorie' => $categorie,
            'form' => $form->createView(),
        ]);
    }


    /**
     * disable station.
     *
     * @Route("/{id}/disable", name="categorie_disable", methods={"GET"})
     */
    public function disableAction(Categories $categorie)
    {
        $em = $this->getDoctrine()->getManager();
        $onecategorie =$em->getRepository('WebBundle:Categories')->find($categorie->getId());
        $onecategorie->setIsActive(false);
        $em->persist($onecategorie);
        $em->flush();
        return $this->redirectToRoute('categorie_index');
    }
    /**
     * @Route("/Liste_des_categories", name="ExportCategorie" ,defaults={"_format"="xls","_filename"="Liste_des_categories"}, requirements={"_format"="csv|xls|xlsx"})
     * @Template("@Backend/excel/CategorieExcel.xlsx.twig")
     */
    public function ExportModeleAction($_filename, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        parse_str(parse_url($this->getRequest()->headers->get('referer'), PHP_URL_QUERY), $output);

        if(array_key_exists('search',$output)) $search=$output['search']; else $search='';

        $categories =$em->getRepository('WebBundle:Categories')->getCategorieByPays($this->getUser()->getPays(),$search);
        return $this->render('@Backend/excel/CategorieExcel.xlsx.twig', array(
            'Modeles' => $categories,
        ));
    }

    }
