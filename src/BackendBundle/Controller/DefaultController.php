<?php

namespace BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use WebBundle\Entity\Utilisateurs;

class DefaultController extends Controller
{

    /**
     * @Route("/dashboard", name= "admin_dashboard")
     */
    public function indexAction()
    {
        $authChecker = $this->container->get('security.authorization_checker');
        if ($authChecker->isGranted('ROLE_SUPER_ADMIN')) {
            return $this->redirectToRoute('pays_index');
        }
        elseif ($authChecker->isGranted('ROLE_GESTIONNAIRE')) {
            return $this->redirectToRoute('dashboard_index');
        }
        elseif ($authChecker->isGranted('ROLE_GERANT')) {
            return $this->redirectToRoute('dashboard_gerant_index');
        }
        else {
            return $this->redirectToRoute('logout');
        }
    }

}
