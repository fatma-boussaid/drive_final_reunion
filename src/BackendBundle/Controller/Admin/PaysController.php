<?php

namespace BackendBundle\Controller\Admin;


use BackendBundle\Form\PaysType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use WebBundle\Entity\Pays;
use WebBundle\Repository\PaysRepository;
use Symfony\Component\Intl\Intl;
/**
 * @Route("/pays")
 */
class PaysController extends Controller
{
    /**
     * @Route("/", name="pays_index", methods={"GET"})
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $code=$request->get('code',"");

        $pays =$em->getRepository('WebBundle:Pays')->getAllPays($code);
        return $this->render('@Backend/admin/pays/index.html.twig', [
            'code' => $request->get('code'),
            'pays' => $pays,
            'listpays' => $em->getRepository('WebBundle:Pays')->findAll()
        ]);





       // return $this->render('security/login.html.twig');
    }

    /**
     * @Route("/new", name="pays_new", methods={"GET","POST"})
     */
    public function newAction(Request $request)
    {   $countries =  Intl::getRegionBundle()->getCountryNames();
        $codes=array();
        foreach ($countries as $key =>$value)
        {
          $codes[strtolower($key)] = $value;
        }
        $formOptions = array('code' => $codes);
        $pay = new Pays();
        $form = $this->createForm(PaysType::class, $pay,$formOptions);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($pay);
            $entityManager->flush();

            return $this->redirectToRoute('pays_index');
        }

        return $this->render('@Backend/admin/pays/new.html.twig', [
            'pay' => $pay,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="pays_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Pays $pay)
    {
        $countries = Intl::getRegionBundle()->getCountryNames();
        $codes = array();
        foreach ($countries as $key => $value) {
            $codes[strtolower($key)] = $value;
        }
        $formOptions = array('code' => $codes);
        $form = $this->createForm(PaysType::class, $pay,$formOptions);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('pays_index');
        }

        return $this->render('@Backend/admin/pays/edit.html.twig', [
            'pay' => $pay,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="pays_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Pays $pay)
    {
        if ($this->isCsrfTokenValid('delete'.$pay->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($pay);
            $entityManager->flush();
        }

        return $this->redirectToRoute('pays_index');
    }

    /**
     * enable pays.
     *
     * @Route("/{id}/enable", name="pays_enable", methods={"GET"})
     */
    public function enableAction(Pays $pays)
    { $em = $this->getDoctrine()->getManager();

        $em = $this->getDoctrine()->getManager();
        $onepays=$em->getRepository('WebBundle:Pays')->find($pays->getId());
        $onepays->setIsActive(true);
        $em->persist($onepays);
        $em->flush();
        return $this->redirectToRoute('pays_index');
    }

    /**
     * disable pays.
     *
     * @Route("/{id}/disable", name="pays_disable", methods={"GET"})
     */
    public function disableAction(Pays $pays,PaysRepository $paysRepository)
    {
        $em = $this->getDoctrine()->getManager();
        $onepays=$paysRepository->find($pays->getId());
        $onepays->setIsActive(false);
        $em->persist($onepays);
        $em->flush();
        return $this->redirectToRoute('pays_index');
    }
}
