<?php

namespace FrontendBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/produit")
 */
class ProduitController extends BaseController
{
    /**
     * @Route(path="/", name="list_produit")
     */
    public function indexAction(Request $request)
    {
        $this->initEntityManager();
        $this->initSession();
        $this->initStation();


        $categorie_id = $this->getRequest()->query->get('categorie_id', "");
        $sub_categorie_id = $this->getRequest()->query->get('sub_categorie_id', "");
        $unite_id = $this->getRequest()->query->get('unite_id', "");
        $keyword = $this->getRequest()->query->get('keyword', "");
        $max_price = $this->getRequest()->query->get('max_price', "");
        $min_price = $this->getRequest()->query->get('min_price', "");
        $sort = $this->getRequest()->query->get('sort', "");
        $station_id = $this->session->get("station_id", "");
        switch ($sort) {
            case 1:
                $sort_by = "created_at";
                break;
            case 2:
                $sort_by = "nb_achat";
                break;
            case 3:
                $sort_by = "prix";
                break;
            default:
                $sort_by = "";
        }


        //$categories = $this->getAllCategories();
        // $unites = $this->getAllUnites();

        if ($categorie_id == "" && $sub_categorie_id == "" && $keyword == "") {
            $categorie_id = -1;
            $sub_categorie_id = -1;
        }


        $findProduits = $this->getProduitsFiltred($unite_id, $categorie_id, $min_price, $max_price, $sort_by, $keyword, $sub_categorie_id, $station_id);
        $produits = $this->get('knp_paginator')->paginate($findProduits, $this->get('request')->query->get('page', 1), 16);


        if ($this->session->has('panier'))
            $panier = $this->session->get('panier');
        else
            $panier = false;

        $request = $this->container->get('request');
        $routeName = $request->get('_route');


        if ($categorie_id == "" && $sub_categorie_id != "") {
            $categorie_id = $this->em->getRepository("WebBundle:SousCategories")->findOneById($sub_categorie_id)->getCategorieId();
        }


        $sous_categories = $this->em->getRepository("WebBundle:SousCategories")->getListSousCategorie("re",$this->station_id,$categorie_id);
        return $this->render('FrontendBundle:Produit:index.html.twig', array('produits' => $produits,
            'panier' => $panier, 'cp' => $routeName, "sous_categories" => $sous_categories));
    }


    /**
     * @Route(path="/{id}", name="show_produit")
     */
    public function showAction(Request $request, $id)
    {
        $em = $this->initEntityManager();
        $produit = $this->getOneProduitById($id);
        $produits = $this->getProduitBy(array("categorie_id" => $produit->getCategorieId()));
        $produits_similaire = $this->get('knp_paginator')->paginate($produits, $this->get('request')->query->get('page', 1), 4);

        return $this->render('FrontendBundle:Produit:show.html.twig', array("produit" => $produit, "produits_similaire" => $produits_similaire, 'cp' => "produitsPage"));
    }

}
