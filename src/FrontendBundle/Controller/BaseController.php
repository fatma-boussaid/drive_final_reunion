<?php

namespace FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class BaseController extends Controller
{

    public $em = null;
    public $session = null;
    public $panier = null;
    public $adresse = null;
    public $commande = null;
    public $station_id = null;


    public function initSession()
    {
        if (is_null($this->session))
            $this->session = $this->getRequest()->getSession();
        return $this->session;
    }

    public function initEntityManager()
    {
        if (is_null($this->em))
            $this->em = $this->getDoctrine()->getManager();
        return $this->em;
    }

    public function initPanier()
    {
        if (!$this->session->has('panier')) $this->session->set('panier', array());

        $this->panier = $this->session->get('panier');
        return $this->panier;
    }

    public function initAdresse()
    {
        if (!$this->session->has('adresse')) $this->session->set('adresse', array());

        $this->adresse = $this->session->get('adresse');
        return $this->adresse;
    }
    public function initStation()
    {
        if (!$this->session->has('station_id')) $this->session->set('station_id', "");

        $this->station_id = $this->session->get('station_id');
        return $this->station_id;
    }

    public function initDate()
    {
        if (!$this->session->has('date')) $this->session->set('date', array());
        $this->adresse = $this->session->get('date');
        return $this->adresse;
    }

    public function initCommande()
    {
        if (!$this->session->has('commande')) $this->session->set('commande', array());

        $this->commande = $this->session->get('commande');
        return $this->commande;
    }


    public function getProduitBy($params)
    {
        return $this->em->getRepository('WebBundle:Produits')->findBy($params);
    }

    public function getOneProduitById($id)
    {
        return $this->em->getRepository('WebBundle:Produits')->findOneById($id);
    }

    public function getProduitsFiltred($unite_id,$categorie_id, $min_price, $max_price, $sort,$keyword,$sub_categorie_id="",$station_id="")
    {
        return $this->em->getRepository('WebBundle:Produits')->getProduitsFiltred($unite_id,$categorie_id, $min_price, $max_price, $sort,$keyword,$sub_categorie_id, $station_id);
    }

    public function getProduitsByIds($ids)
    {
        return $this->em->getRepository('WebBundle:Produits')->getProduitsByIds($ids);
    }

    public function getAllCategories()
    {
        return $this->em->getRepository('WebBundle:Categories')->findAll();
    }
    public function getAllUnites()
    {
        return $this->em->getRepository('WebBundle:Unite')->findAll();
    }


}
