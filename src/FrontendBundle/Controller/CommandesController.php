<?php

namespace FrontendBundle\Controller;

use DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use WebBundle\Entity\Commande_Produit;
use WebBundle\Entity\Commandes;
use WebBundle\Entity\Commandes_grossiste;

class CommandesController extends BaseController
{
    public function facture($c)
    {


        $em = $this->getDoctrine()->getManager();
        $generator = $this->container->get('security.secure_random');

        $session = $this->getRequest()->getSession();
        $panier = $session->get('panier');
        $commande = $this->commande;
        $totalHT = 0;
        $stockproduit = 0;

        $produits = $em->getRepository('WebBundle:Produits')->findArray(array_keys($panier));


        foreach ($produits as $produit) {
            if (($produit->getQuantite() -  $panier[$produit->getId()]) >= 0) {
                $prixHT = $produit->getTotalPrice($panier[$produit->getId()]) ;
                $totalHT += $prixHT["prix"];
                $commande['produit'][$produit->getId()] = array(
                    'id' => $produit->getId(),
                    'categorie' => $produit->getCategorie()->getNom(),
                    'souscategorie' => $produit->getSousCategorie()->getNom(),
                    'nom' => $produit->getNom(),
                    'quantite' => $panier[$produit->getId()],
                    'prix' => round($produit->getPrix(), 2),
                    'image' => $produit->getImage()
                );
                $cp = new Commande_Produit();
                $cp->setProduit($produit);
                $cp->setQuantite($panier[$produit->getId()]);
                $cp->setCommande($c);
                $cp->setPrix($prixHT["prix"]);
              //  $produit->setQuantite($produit->getQuantite() - $panier[$produit->getId()]);
                $em->persist($produit);
                $em->persist($cp);

            } else {
                if($produit->getQuantite() == 0)
                {
                    $this->get('session')->getFlashBag()->add('produitfini', 'Le produit '.$produit->getNom().' est hors stock');
                }
                else{
                    $this->get('session')->getFlashBag()->add('produitfini', 'Il ne reste que ' . $produit->getQuantite() . ' '.$produit->getNom());

                }
                $stockproduit++;
            }
        }


        $c->setPrix(round($totalHT, 2));

        $em->merge($c);


        if ($stockproduit == 0) {
            $em->flush();
            $c->setReference($c->getId() + 1000);
            $em->flush();

            $commande['reference'] = $c->getReference();
            $commande['prix'] = round($totalHT, 2);
            $commande['id'] = $c->getId();
            $commande['token'] = bin2hex($generator->nextBytes(20));
        } else {
            $commande = null;
        }


        return $commande;
    }


    public function prepareCommandeAction()
    {

        $session = $this->initSession();
        $em = $this->initEntityManager();
        $date = $this->initDate();
        $station_id = $this->initStation();

        //var_dump($session->get('commande'));
        //exit();
        if (!$session->has('commande') || !isset($session->get("commande", array())["id"])) {
            //var_dump($session->has('commande'));exit();
            $commande = new Commandes();
            //var_dump($commande);exit();
        } else {
            //var_dump($session->get('commande'));exit();
            $commande = $em->getRepository('WebBundle:Commandes')->find($session->get('commande'));
            //var_dump($commande);exit();
        }


        //$commande = new Commandes();
        // var_dump($commande);die();
        $commande->setUtilisateur($this->getUser());
        $commande->setValider(0);
        $commande->setReference(0);
        // $commande->setCommande($this->facture());
        $commande->setStatutpayement(0);
        $commande->setDateReception(\DateTime::createFromFormat('Y-m-d', $date["date"]));
        $commande->setPeriodeReception($date['periode']);
        $station = $em->getRepository('WebBundle:Station')->find($station_id);
        $commande->setStation($station);
        $commande->setPays($station->getPays());


        $c = $this->facture($commande);

        if ($c) {
            $this->session->set('commande', $c);
            return new Response($c['id'] );
        } else {
            return new Response(null);
        }
    }

    /*
     * Cette methode remplace l'api banque.
     */
    /**
     * @Route(path="/mail_test/{id}",  name="send_mail_test")
     */
    public function sendMailTestAction($id)
    {
        $em = $this->initEntityManager();
        $commande = $em->getRepository('WebBundle:Commandes')->find($id);
        //Ici le mail de validation
        $message = \Swift_Message::newInstance()
            ->setSubject('Validation de votre commande')
            ->setFrom(array('station.drive.re@gmail.com' => "Total B2B"))
            ->setTo("yosri.mekni.sidratsoft@gmail.com")
            ->setCharset('utf-8')
            ->setContentType('text/html')
            ->setBody($this->renderView('FrontendBundle:SwiftLayout:validationCommande.html.twig', array('commande' => $commande,)), 'text/html');

        $this->get('mailer')->send($message);
        // $result = $mailer->send($message);
        //var_dump($result);die();
        return $this->render('FrontendBundle:SwiftLayout:validationCommande.html.twig'
            , array('commande' => $commande));
    }

    /**
     * @Route(path="/validation_commande",  name="validation_commande")
     */
    public function validationCommandeAction(Request $request)
    {

        $payzen_response = json_decode($request->get("kr-answer", "{}"));


        $status_payement = 0;
        if (property_exists($payzen_response, "orderStatus") && $payzen_response->orderStatus == "PAID") {
            $status_payement = 1;
        }

        // $payzen_response = json_decode($payzen_response);

        $em = $this->initEntityManager();
        $session = $this->initSession();
        $s_commande = $this->initCommande();
        if(!isset($s_commande["id"])){
            return $this->redirectToRoute('homepage_frontend');
        }

        $id = $s_commande["id"];
        $panier = $this->initPanier();
        $adresse = $this->initAdresse();
        $csession = $this->initCommande();
        /**
         * @var Commandes $commande
         */
        $commande = $em->getRepository('WebBundle:Commandes')->find( $id );
        $gerant=$em->getRepository('WebBundle:Utilisateurs')->getGerantByStation( $commande->getStation() );
        $commande->setValider($status_payement);
        $commande->setStatutpayement($status_payement);
        $commande->setPayzenResponse($payzen_response);
        $commande->setDateReservation(new DateTime('now'));


        $commande_valid = $session->get('commande');
        $produits = $em->getRepository("WebBundle:Commande_Produit")->findBy(array('commande' => $commande));


        if ($status_payement == 1) {
            $i=0;
            $message='Attention, dans votre stock, il ne vous reste que ';
            foreach ($produits as $produit) {

                $produit->getProduit()->setQuantite($produit->getProduit()->getQuantite() -$produit->getQuantite());

                if ($produit->getProduit()->getQuantite() <= 3) {
                    $i++;
                    $message=$message . $produit->getProduit()->getQuantite() . ' ' . $produit->getProduit()->getNom() .', ';
                }

                $em->merge($produit);
            }
            if($i>0){
                $gerant = $em->getRepository('WebBundle:Utilisateurs')->getGerantByStation($commande->getStation());
                $message_gerant = \Swift_Message::newInstance()
                    ->setSubject('Alert Stock')
                    ->setFrom(array('station.drive.re@gmail.com' => "Drive"))
                    ->setTo($gerant->getEmailCanonical())
                    ->setCharset('utf-8')
                    ->setContentType('text/html')
                    ->setBody( $message);
                $this->get('mailer')->send($message_gerant);
            }
            $message_client = \Swift_Message::newInstance()
                ->setSubject('Commande Validée')
                ->setFrom(array('station.drive.re@gmail.com' => "Drive"))
                ->setTo($commande->getUtilisateur()->getEmailCanonical())
                ->setCharset('utf-8')
                ->setContentType('text/html')
                ->setBody($this->renderView('FrontendBundle:SwiftLayout:validationCommande.html.twig',
                    array('commande' => $commande, 'produits' => $produits,'email'=>$gerant->getEmailCanonical())));
            $message_gerant = \Swift_Message::newInstance()
                ->setSubject('Nouvelle Commande')
                ->setFrom(array('b2b.total@gmail.com' => "Drive"))
                ->setTo($gerant->getEmailCanonical())
                ->setCharset('utf-8')
                ->setContentType('text/html')
                ->setBody($this->renderView('FrontendBundle:SwiftLayout:nouvelleCommande.html.twig',
                    array('client' => $this->getUser(),'commande' => $commande, 'produits' => $produits)));
            $this->get('mailer')->send($message_client);
            $this->get('mailer')->send($message_gerant);

            $p = $panier;
            $panier = [];
            $csession = [];
            $adresse = [];
            $this->session->set('panier', $panier);
            $this->session->remove('commande');
        }

        $em->flush();
        return $this->render(
            'FrontendBundle:panier:thanks.html.twig',
            array(
                'produits' => $produits,
                'totale' => $commande->getPrix(),
                "reference" => $commande->getReference(),
                "message" => null,
                'last_username' => null,
                'error' => null,
                'csrf_token' => null,
            )
        );
    }
}
