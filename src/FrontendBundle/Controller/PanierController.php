<?php

namespace FrontendBundle\Controller;

use FrontendBundle\Form\UtilisateursAdressesType;
use Lyra\Client;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use WebBundle\Entity\Commande_Produit;
use WebBundle\Entity\UtilisateursAdresses;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class PanierController extends BaseController
{
    public function menuAction()
    {
        $this->initSession();
        $this->initPanier();
        $this->initEntityManager();
        $this->initStation();
        //dump($this->session->clear());die;
        $articles = $this->getProduitsByIds(array_keys($this->panier));
        $categories_collection = $this->em->getRepository("WebBundle:Categories")->getAllCategories("re",$this->station_id);
        $categories = array("auto"=>array(),"food"=>array(),"other"=>array());
        foreach ($categories_collection as $category) {
            if ($category->isFood()) {
                $categories["food"][] = $category;
            }
            elseif ($category->isAuto()){
                $categories["auto"][] = $category;
            }
            else {
                $categories["other"][] = $category;
            }
        }


        $stations = $this->getDoctrine()->getRepository('WebBundle:Station')->findBy(array("isActive" => 1,"isDeleted" => 0, "pays" => 3));
        $stations_array = array();
        foreach ($stations as $station) {
            $stations_array[$station->getZone()][] = $station;
        }
        $station = $this->getDoctrine()->getRepository('WebBundle:Station')->find($this->station_id);



        return $this->render('FrontendBundle:Blocks:navbar.html.twig', array('station' => $station,'panier' => $this->panier, 'articles' => $articles, "categories" => $categories, "stations" => $stations_array));
    }

    /**
     * @Route(path="/delete_item_from_cart/{id}",options={"expose"=true}, name="delete_item_from_cart")
     */
    public function supprimerAction($id)
    {

        $this->initSession();
        $this->initPanier();

        if (array_key_exists($id, $this->panier)) {
            unset($this->panier[$id]);
            $this->session->set('panier', $this->panier);
            //$this->get('session')->getFlashBag()->add('success', 'Article supprimé avec succès');
        }

        return $this->menuAction();
    }

    /**
     * @Route(path="/add_item_to_cart/{id}",options={"expose"=true},  name="add_item_to_cart")
     */
    public function ajouterAction($id)
    {
        $this->initSession();
        $this->initPanier();

        $panier = $this->panier;

        if (array_key_exists($id, $panier)) {
            $panier[$id]++;
            //$this->get('session')->getFlashBag()->add('success','Quantité modifié avec succès');
        } else {
            if (is_null($this->getRequest()->query->get('qte')))
                $panier[$id] = 1;
            else
                $panier[$id] = $this->getRequest()->query->get('qte');
            // $this->get('session')->getFlashBag()->add('success', 'Article ajouté avec succès');
        }

        $this->session->set('panier', $panier);

        return $this->menuAction();
    }

    /**
     * @Route(path="/delete_quantity_to_cart/{id}",options={"expose"=true},  name="delete_quantity_to_cart")
     */
    public function quantiydeleteAction($id)
    {
        $this->initSession();
        $this->initPanier();

        $panier = $this->panier;

        if (array_key_exists($id, $panier)) {
            $quantite = $panier[$id] - 1;
            if ($quantite == 0) {
                unset($panier[$id]);
                $this->session->set('panier', $panier);
            } else if ($quantite > 0) {
                $panier[$id]--;
            }

            //$this->get('session')->getFlashBag()->add('success','Quantité modifié avec succès');
        }

        $this->session->set('panier', $panier);

        return $this->menuAction();
    }

    /**
     * @Route(path="/add_quantity_to_cart/{id}",options={"expose"=true},  name="add_quantity_to_cart")
     */
    public function quantiyaddAction($id)
    {
        $this->initSession();
        $this->initPanier();

        $panier = $this->panier;

        if (array_key_exists($id, $panier)) {
            $panier[$id]++;
            //$this->get('session')->getFlashBag()->add('success','Quantité modifié avec succès');
        } else {
            if (is_null($this->getRequest()->query->get('qte')))
                $panier[$id] = 1;
            else
                $panier[$id] = $this->getRequest()->query->get('qte');
            // $this->get('session')->getFlashBag()->add('success', 'Article ajouté avec succès');
        }

        $this->session->set('panier', $panier);

        return $this->menuAction();
    }

    /**
     * @Route(path="/update_cart",options={"expose"=true},  name="update_cart")
     */
    public function updateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $this->initSession();
        $panier = $this->initPanier();
        $qts = $request->query->get('qte');
        $i = 0;
        foreach ($panier as $key => $value) {
            $produit = $em->getRepository('WebBundle:Produits')->find($key);
            if($qts[$i]>$produit->getQuantite()){
                if($produit->getQuantite() == 0)
                {
                    $this->get('session')->getFlashBag()->add('produitfini', 'Le produit '.$produit->getNom().' est hors stock');

                }
                else{
                    $this->get('session')->getFlashBag()->add('produitfini', 'Il ne reste que ' . $produit->getQuantite() . ' '.$produit->getNom());

                }
            }
            else{
                $panier[$key] = $qts[$i];

            }
            $i++;

        }
        $this->session->set('panier', $panier);

        return $this->redirect($this->generateUrl('panier'));
    }


    /**
     * @Route(path="/panier",  name="panier")
     */
    public function panierAction()
    {
        $this->initSession();
        $panier = $this->initPanier();
        $date = $this->initDate();
        $this->initEntityManager();
//$this->panier['256'] =35;
//dump($this->panier);die;
        $produits = $this->getProduitsByIds(array_keys($this->panier));

        return $this->render('FrontendBundle:panier:panier.html.twig',
            array(
                'produits' => $produits,
                'panier' => $panier,
                "date" => $date
            ));
    }

    /**
     * @Route(path="/deleteAdresse/{id}",  name="deleteAdresse")
     */
    public function adresseSuppressionAction(Request $request, $id)
    {
        $em = $this->initEntityManager();
        $entity = $em->getRepository('WebBundle:UtilisateursAdresses')->find($id);

        if ($this->getUser() != $entity->getUtilisateur() || !$entity)
            return $this->redirect($this->generateUrl('livraison'));

        $em->remove($entity);
        $em->flush();

        if ($request->get("from_profile", 0) == 1)
            return $this->redirect($this->generateUrl('profile_adresse'));

        return $this->redirect($this->generateUrl('livraison'));
    }
    /**
     * @Route(path="/verification_panier",options={"expose"=true}, name="verification_panier")
     */
    public function verifPanierAction(Request $request)
    {
        $this->initSession();
        $panier = $this->initPanier();
        $date = $this->initDate();
        $em = $this->initEntityManager();
        $produits = $this->getProduitsByIds(array_keys($this->panier));
        $stockproduit=0;
        foreach ($produits as $produit) {

                if($produit->getQuantite() == 0)
                {
                    $this->get('session')->getFlashBag()->add('produitfini', 'Le produit '.$produit->getNom().' est hors stock');
                    $stockproduit++;
                }
                else if($produit->getQuantite()< $panier[$produit->getId()]){
                    $this->get('session')->getFlashBag()->add('produitfini', 'Il ne reste que ' . $produit->getQuantite() . ' '.$produit->getNom());
                    $stockproduit++;
                }


        }

        if($stockproduit == 0)return $this->redirect($this->generateUrl('livraison'));
        else return $this->redirect($this->generateUrl('panier'));


    }

    /**
     * @Route(path="/livraison",  name="livraison")
     */
    public function livraisonAction(Request $request)
    {
        $this->initSession();
        $panier = $this->initPanier();
        $date = $this->initDate();
        $em = $this->initEntityManager();
        $utilisateur = $this->getUser();
        $entity = new UtilisateursAdresses();
        $form = $this->createForm(new UtilisateursAdressesType($em), $entity);


        if ($this->get('request')->getMethod() == 'POST') {
            $form->handleRequest($this->getRequest());
            //if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setUtilisateur($utilisateur);

            $delegation = $em->getRepository('WebBundle:Delegation')->findOneBy(array('code' => $entity->getCp()));
            if ($delegation) {
                $entity->setPays($delegation->getRegion()->getName());
                $entity->setVille($delegation->getName());
            }

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('livraison'));
            //}
        }

        $station_id = $this->initStation();
        $station = $em->getRepository("WebBundle:Station")->find($station_id);
        return $this->render('FrontendBundle:panier:livraison.html.twig', array('utilisateur' => $utilisateur,
            'form' => $form->createView(), 'panier' => $panier, "station" => $station));
    }

    public function setLivraisonOnSession($livraison, $facturation)
    {
        $session = $this->getRequest()->getSession();
        //var_dump($livraison);die;
        if (!$session->has('adresse')) $session->set('adresse', array());
        $adresse = $session->get('adresse');


        if ($livraison != null && $facturation != null) {
            $adresse['livraison'] = $livraison;
            $adresse['facturation'] = $facturation;


        } else {
            return $this->redirect($this->generateUrl('validation'));
        }

        $session->set('adresse', $adresse);
        return $this->redirect($this->generateUrl('validationAdresse'));
    }


    /**
     * @Route(path="/validationAdresse",  name="validationAdresse")
     */
    public function validationAction(Request $request)
    {
        $periode = $request->get("periode", 1);

        //$livraison = $request->get("livraison");
        //$facturation = $request->get("facturation");

        $date = $request->get("date", date("Y-m-d"));
        $session = $this->initSession();
        $session_date = $this->initDate();
        $station_id = $this->initStation();

        if ($station_id == "") {
            return $this->redirectToRoute('homepage_frontend');
        }

        if ($request->query->has("periode") && $request->query->has("date")) {
            $session_date["periode"] = $periode;
            $session_date["date"] = $date;
            $this->session->set("date", $session_date);
        }

        //if ($this->get('request')->getMethod() == 'GET')
        //  $this->setLivraisonOnSession($livraison,$facturation);
        $em = $this->getDoctrine()->getManager();

        $prepareCommande = $this->forward('FrontendBundle:Commandes:prepareCommande');


        // $commande = $em->getRepository('WebBundle:Commandes')->find($prepareCommande->getContent());

        if ($this->session->get('commande')['id']) {

            $commande = $em->getRepository("WebBundle:Commandes")->find($this->session->get('commande')['id']);
            $station = $em->getRepository("WebBundle:Station")->find($session->get("station_id", ""));
            $produits = $em->getRepository("WebBundle:Commande_Produit")->GetProduitByCommande( $commande);
            $gerant = $em->getRepository('WebBundle:Utilisateurs')->getGerantByStation($commande->getStation());

            return $this->render('FrontendBundle:panier:validation.html.twig',
                array(
                    'periode' => $periode,
                    'commande' => $commande,
                    "station" => $station,
                    'produits' => $produits,
                     'email'=>$gerant->getEmailCanonical()
                ));

        } else {
            return $this->redirect($this->generateUrl('panier'));
        }
    }


    /**
     * @Route(path="/delegations/{cp}",options={"expose"=true},  name="delegations")
     */
    public function delegationsAction(Request $request, $cp)
    {
        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $villeCodePostal = $em->getRepository('WebBundle:Delegation')->findBy(array('code' => $cp));

            if ($villeCodePostal) {
                $villes = array();
                foreach ($villeCodePostal as $ville) {
                    $villes[str_replace("  ", "", $ville->getName())] = $ville->getName();
                }
            } else {
                $villes = null;
            }

            $response = new JsonResponse();
            //var_dump($response->setData(array('ville' => $villes)));die();
            return $response->setData(array('ville' => $villes));
        } else {
            throw new \Exception('Erreur');
        }
    }


    /**
     * @Route(path="/payment",  name="payement_commande")
     */
    public function PayementCommandeAction()
    {

        $session = $this->initSession();
        $panier = $this->initPanier();
        $s_commande = $this->initCommande();

        if (!isset($s_commande["id"])) {
            return $this->redirectToRoute('homepage_frontend');
        }
        $id = $s_commande["id"];

        require_once __DIR__ . '/../../../vendor/lyracom/rest-php-sdk/src/autoload.php';
        $client = new Client();
        /* Username, password and endpoint used for server to server web-service calls */


        //MODE TEST
        $client->setUsername("81181512");
        $client->setPassword("testpassword_oYQ6zgrzhT10g9ThrhA562n5TaUXW4mDZ15V1omInilum");
        $client->setEndpoint("https://api.payzen.eu");
        $client->setPublicKey("81181512:testpublickey_hUBZKbn4dd6hSEPQIQr3y4JUOQXoaTjMwrRWEEK9MA3MN");
        $client->setSHA256Key("0VyOVIVhaXZFtRMilhQkKOfP3VwrZ2EqOUsPoSXmQffUM");


        //MODE PROD
      /*  $client->setUsername("52351668");
        $client->setPassword("prodpassword_CqSkdjHbVht06Q0GHfwQZKgnsbDKYZYxv7YJzlAqDE7eD");
        $client->setEndpoint("https://api.payzen.eu");
        $client->setPublicKey("52351668:publickey_gDEw4jduFHGxAnAQ0SHgO7eVcasqy9TZMnfIIqbfv6Zxg");
        $client->setSHA256Key("ZvaqQoQQksWlAt3Q6h5ZAk5CnvPSyl0oEfvcF9XGivuDE");*/




        $em = $this->initEntityManager();
        $commande = $em->getRepository('WebBundle:Commandes')->find($id);


        if (!$commande || $commande->getValider() == 1)
            throw $this->createNotFoundException('La commande n\'existe pas');

        // $commande->setValider(1);
        // $commande->setReference($this->container->get('setNewReference')->reference()); //Service
        // $em->flush();


        $produits = $this->getProduitsByIds(array_keys($panier));


        /**
         * I create a formToken
         * @var Commandes $commande
         */
        $price = $commande->getPrix();


        $store = array("amount" => $price * 100,
            "currency" => "EUR",
            "orderId" => "DRIVE_" . $commande->getReference(),
            "customer" => array("email" => $commande->getUtilisateur()->getEmail()
            ));

        $response = $client->post("V4/Charge/CreatePayment", $store);


        /* I check if there are some errors */
        if ($response['status'] != 'SUCCESS') {
            /* an error occurs, I throw an exception */
            $message = json_encode($response);
            exec("echo $message >> payzen_error.txt");

            $error = $response['answer'];
            throw new \Exception("error " . $error['errorCode'] . ": " . $error['errorMessage']);
        }


        $formToken = $response["answer"]["formToken"];


        return $this->render('FrontendBundle:panier:payment.html.twig',
            array(
                'commande' => $commande,
                'produits' => $produits,
                'total_price' => $commande->getPrix(),
                'panier' => $panier,
                "reference" => $commande->getReference(),
                "client" => $client,
                "formToken" => $formToken
            ));
    }

    private function store_error($response)
    {
        $message = "=========";
        foreach ($response as $key => $value) {
            $message .= "- $key = $value ||";
        }
        dump($message);
        die;
        exec("echo $message >> payzen_error.txt");
    }
}
