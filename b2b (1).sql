-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 08, 2020 at 10:01 AM
-- Server version: 5.6.35
-- PHP Version: 7.0.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `b2b`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `nom` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `pays_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `nom`, `pays_id`, `created_at`, `updated_at`, `is_active`) VALUES
(1, 'ELF', NULL, '0000-00-00 00:00:00', NULL, 0),
(2, 'TOTAL', NULL, '0000-00-00 00:00:00', NULL, 0),
(3, 'Produit Alimentaire', 3, '2020-04-02 02:52:19', NULL, 1),
(4, 'Entretien et Nettoyage', 3, '2020-04-02 02:53:31', NULL, 1),
(5, 'legumes et fruits', 3, '2020-04-07 12:45:00', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `commandes`
--

CREATE TABLE `commandes` (
  `id` int(11) NOT NULL,
  `utilisateur_id` int(11) NOT NULL,
  `valider` tinyint(1) NOT NULL,
  `reference` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `date_reservation` datetime DEFAULT NULL,
  `periode_reservation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `delivery_date` datetime DEFAULT NULL,
  `date_validation` datetime DEFAULT NULL,
  `pays_id` int(11) DEFAULT NULL,
  `station_id` int(11) DEFAULT NULL,
  `date_reception` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `commandes`
--

INSERT INTO `commandes` (`id`, `utilisateur_id`, `valider`, `reference`, `created_at`, `date_reservation`, `periode_reservation`, `status`, `delivery_date`, `date_validation`, `pays_id`, `station_id`, `date_reception`) VALUES
(20, 31, 1, 12465478, '2020-04-01 00:00:00', '2020-04-01 12:30:00', '', 2, NULL, '2020-04-01 13:00:00', 3, 1, '2020-04-01 17:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `commande_produit`
--

CREATE TABLE `commande_produit` (
  `id` int(11) NOT NULL,
  `commande_id` int(11) DEFAULT NULL,
  `produit_id` int(11) DEFAULT NULL,
  `quantite` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `commande_produit`
--

INSERT INTO `commande_produit` (`id`, `commande_id`, `produit_id`, `quantite`) VALUES
(1, 20, 54, 3),
(2, 20, 55, 22);

-- --------------------------------------------------------

--
-- Table structure for table `delegation`
--

CREATE TABLE `delegation` (
  `id` int(11) NOT NULL,
  `grossiste_id` int(11) DEFAULT NULL,
  `region_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `delegation`
--

INSERT INTO `delegation` (`id`, `grossiste_id`, `region_id`, `name`, `code`, `status`) VALUES
(2, 7, 23, 'TUNIS RECETTE PRINCIPALE', '1000', 'TUNIS'),
(3, 7, 23, 'TUNIS REPUBLIQUE    ', '1001', 'TUNIS'),
(4, 7, 23, 'TUNIS BELVEDERE     ', '1002', 'TUNIS'),
(5, 7, 23, 'CITE EL KHADRA      ', '1003', 'TUNIS'),
(6, 7, 23, 'EL MENZAH           ', '1004', 'TUNIS'),
(7, 7, 23, 'EL OMRANE           ', '1005', 'TUNIS'),
(8, 7, 23, 'TUNIS BAB SOUIKA    ', '1006', 'TUNIS'),
(9, 7, 23, 'TUNIS JABBARI       ', '1007', 'TUNIS'),
(10, 7, 23, 'TUNIS BAB MENARA    ', '1008', 'TUNIS'),
(11, 7, 23, 'EL OUARDIA          ', '1009', 'TUNIS'),
(12, 7, 23, 'MENZAH 9            ', '1013', 'TUNIS'),
(13, 7, 23, 'BAD                 ', '1017', 'TUNIS'),
(14, 7, 23, 'TUNIS EL MEDINA     ', '1018', 'TUNIS'),
(15, 7, 23, 'TUNIS BAB BNET      ', '1019', 'TUNIS'),
(16, 7, 23, 'TUNIS MOHAMED V     ', '1023', 'TUNIS'),
(17, 7, 23, 'TUNIS BEB EL FALLA  ', '1027', 'TUNIS'),
(18, 7, 23, 'BAB SAADOUN GARE    ', '1029', 'TUNIS'),
(19, 7, 23, 'MAISON DES SERVICES AIN ZAGHOUAN', '1033', 'TUNIS'),
(20, 7, 23, 'DJEBEL JELOUD       ', '1046', 'TUNIS'),
(21, 7, 23, 'TUNIS HACHED        ', '1049', 'TUNIS'),
(22, 7, 23, 'LES BERGES DU LAC   ', '1053', 'TUNIS'),
(23, 7, 23, 'AMILCAR             ', '1054', 'TUNIS'),
(24, 7, 23, 'GAMMART            ', '1057', 'TUNIS'),
(25, 7, 23, 'TUNIS ELHAFSIA      ', '1059', 'TUNIS'),
(26, 7, 23, 'SIDI HASSINE 2      ', '1063', 'TUNIS'),
(27, 7, 23, 'CITE INTILAKA       ', '1064', 'TUNIS'),
(28, 7, 23, 'LA GOULETTE PORT    ', '1067', 'TUNIS'),
(29, 7, 23, 'CITE ROMANA         ', '1068', 'TUNIS'),
(30, 7, 23, 'TUNIS THAMEUR       ', '1069', 'TUNIS'),
(31, 7, 23, 'MONTPLAISIR         ', '1073', 'TUNIS'),
(32, 7, 23, 'EL MOUROUJ 2        ', '1074', 'TUNIS'),
(33, 7, 23, 'TUNIS BAB EL KHADRA ', '1075', 'TUNIS'),
(34, 7, 23, 'MONCEF BEY', '1079', 'TUNIS'),
(35, 7, 23, 'CITE MAHRAJENE      ', '1082', 'TUNIS'),
(36, 7, 23, 'TUNIS CHABBI        ', '1089', 'TUNIS'),
(37, 7, 23, 'EL OMRANE SUPERIEUR ', '1091', 'TUNIS'),
(38, 7, 23, 'SIDI HASSINE        ', '1095', 'TUNIS'),
(39, NULL, 24, 'ZAGHOUAN            ', '1100', 'ZAGHOUAN            '),
(40, NULL, 13, 'MORNAGUIA           ', '1110', 'La Manouba             '),
(41, NULL, 24, 'JEBEL EL OUEST      ', '1111', 'ZAGHOUAN            '),
(42, NULL, 13, 'EL MOUSSAIDINE      ', '1113', 'La Manouba             '),
(43, NULL, 13, 'EL BATHAN           ', '1114', 'La Manouba             '),
(44, NULL, 24, 'SAOUAF              ', '1115', 'ZAGHOUAN            '),
(45, NULL, 13, 'MORNAGUIA 20MARS    ', '1116', 'La Manouba             '),
(46, NULL, 24, 'EL MAGREN           ', '1121', 'ZAGHOUAN            '),
(47, NULL, 24, 'ZRIBA GUARIA        ', '1122', 'ZAGHOUAN            '),
(48, NULL, 13, 'JEDEIDA             ', '1124', 'La Manouba             '),
(49, 7, 3, 'RADES SALINE        ', '1125', 'BEN AROUS           '),
(50, NULL, 13, 'SANHAJA', '1126', 'La Manouba             '),
(51, 1, 1, 'BUREAU VIRTUEL', '1128', 'ARIANA              '),
(52, NULL, 13, 'TEBOURBA            ', '1130', 'La Manouba             '),
(53, NULL, 24, 'SMINJA              ', '1131', 'ZAGHOUAN            '),
(54, NULL, 13, 'CHOUIGUI            ', '1133', 'La Manouba             '),
(55, NULL, 13, 'CHAOUAT             ', '1134', 'La Manouba             '),
(56, NULL, 3, 'NASSEN              ', '1135', 'BEN AROUS           '),
(57, NULL, 13, 'CHABAW', '1137', 'La Manouba             '),
(58, NULL, 24, 'EL FAHS             ', '1140', 'ZAGHOUAN            '),
(59, NULL, 24, 'BIR MECHERGA        ', '1141', 'ZAGHOUAN            '),
(60, NULL, 13, 'BORJ EL AMRI        ', '1142', 'La Manouba             '),
(61, NULL, 13, 'BORJ ETTOUMI        ', '1143', 'La Manouba             '),
(62, NULL, 13, 'EDDEKHILA           ', '1144', 'La Manouba             '),
(63, NULL, 3, 'MOHAMMEDIA          ', '1145', 'BEN AROUS           '),
(64, NULL, 24, 'HAMMAM ZRIBA        ', '1152', 'ZAGHOUAN            '),
(65, NULL, 13, 'EL FEJJA            ', '1153', 'La Manouba             '),
(66, NULL, 24, 'BIR HLIMA           ', '1155', 'ZAGHOUAN            '),
(67, NULL, 24, 'ENNADHOUR           ', '1160', 'ZAGHOUAN            '),
(68, NULL, 24, 'BIR CHAOUCH         ', '1163', 'ZAGHOUAN            '),
(69, NULL, 3, 'HAMMAM ECHATT       ', '1164', 'BEN AROUS           '),
(70, NULL, 24, 'BIR MCHARGA GARE    ', '1193', 'ZAGHOUAN            '),
(71, NULL, 9, 'KASSERINE', '1200', 'KASSERINE           '),
(72, NULL, 9, 'THALA               ', '1210', 'KASSERINE           '),
(73, NULL, 9, 'KHEMOUDA            ', '1212', 'KASSERINE           '),
(74, NULL, 9, 'BOUCHEBKA           ', '1213', 'KASSERINE           '),
(75, NULL, 9, 'MAJEN BEL ABBES     ', '1214', 'KASSERINE           '),
(76, NULL, 9, 'THELEPTE            ', '1215', 'KASSERINE           '),
(77, NULL, 9, 'EL AYOUN            ', '1216', 'KASSERINE           '),
(78, NULL, 9, 'FOUSSANA            ', '1220', 'KASSERINE           '),
(79, NULL, 9, 'HAIDRA              ', '1221', 'KASSERINE           '),
(80, NULL, 9, 'KASSERINE NOUR', '1230', 'KASSERINE           '),
(81, NULL, 9, 'BOUZGUEM            ', '1233', 'KASSERINE           '),
(82, NULL, 9, 'CITE EL OMMEL       ', '1237', 'KASSERINE           '),
(83, NULL, 9, 'FERIANA             ', '1240', 'KASSERINE           '),
(84, NULL, 9, 'HASSI EL FARID      ', '1241', 'KASSERINE           '),
(85, NULL, 9, 'LAHOUACHE           ', '1242', 'KASSERINE           '),
(86, NULL, 9, 'SBEITLA             ', '1250', 'KASSERINE           '),
(87, NULL, 9, 'THALA SUD           ', '1261', 'KASSERINE           '),
(88, NULL, 9, 'CITE ESSOUROUR      ', '1263', 'KASSERINE           '),
(89, NULL, 9, 'SBIBA               ', '1270', 'KASSERINE           '),
(90, NULL, 9, 'KASSERINE EZOUHOUR  ', '1279', 'KASSERINE           '),
(91, NULL, 9, 'JEDLIANE            ', '1280', 'KASSERINE           '),
(92, 7, 23, 'BARDO               ', '2000', 'TUNIS'),
(93, 1, 1, 'CITE ENNASR', '2001', 'ARIANA              '),
(94, 1, 1, 'ARIANA GEANT        ', '2002', 'ARIANA              '),
(95, NULL, 1, 'MAISON DES SERVICES ENNASR', '2004', 'ARIANA              '),
(96, 7, 23, 'KSAR ESSAID         ', '2009', 'TUNIS'),
(97, NULL, 13, 'La Manouba             ', '2010', 'La Manouba             '),
(98, NULL, 13, 'DEN-DEN             ', '2011', 'La Manouba             '),
(99, NULL, 13, 'EL HABIBIA          ', '2012', 'La Manouba             '),
(100, NULL, 3, 'BEN AROUS           ', '2013', 'BEN AROUS           '),
(101, NULL, 3, 'MEGRINE RIADH       ', '2014', 'BEN AROUS           '),
(102, 7, 23, 'KRAM                ', '2015', 'TUNIS'),
(103, 7, 23, 'CARTHAGE            ', '2016', 'TUNIS'),
(104, 7, 23, 'KHAZNADAR           ', '2017', 'TUNIS'),
(105, 7, 3, 'RADES VILLAGE MEDITERRANEENNE ', '2018', 'BEN AROUS           '),
(106, NULL, 1, 'SIDI THABET         ', '2020', 'ARIANA              '),
(107, NULL, 13, 'OUED ELLIL          ', '2021', 'La Manouba             '),
(108, 1, 1, 'KALAAT EL ANDALOUS  ', '2022', 'ARIANA              '),
(109, 7, 23, 'SIDI FATHALLAH      ', '2023', 'TUNIS'),
(110, NULL, 3, 'MEGRINE CHAKER      ', '2024', 'BEN AROUS           '),
(111, 7, 23, 'SALAMMBO            ', '2025', 'TUNIS'),
(112, 7, 23, 'SIDI BOU SAID       ', '2026', 'TUNIS'),
(113, 1, 1, 'BORJ EL BACCOUCHE   ', '2027', 'ARIANA              '),
(114, NULL, 13, 'EL BASSATINE        ', '2028', 'La Manouba             '),
(115, NULL, 13, 'ESSAIDA             ', '2031', 'La Manouba             '),
(116, 1, 1, 'CEBALAT BEN AMMAR   ', '2032', 'ARIANA              '),
(117, NULL, 3, 'MEGRINE             ', '2033', 'BEN AROUS           '),
(118, NULL, 3, 'EZZAHRA             ', '2034', 'BEN AROUS           '),
(119, 1, 1, 'TUNIS CARTHAGE      ', '2035', 'ARIANA              '),
(120, NULL, 1, 'LA SOUKRA           ', '2036', 'ARIANA              '),
(121, NULL, 1, 'MENZAH 8            ', '2037', 'ARIANA              '),
(122, 7, 3, 'RADES               ', '2040', 'BEN AROUS           '),
(123, NULL, 1, 'CITE ETTADHAMEN     ', '2041', 'ARIANA              '),
(124, 7, 23, 'CITE ETTAHRIR       ', '2042', 'TUNIS'),
(125, NULL, 3, 'BEN AROUS SUD       ', '2043', 'BEN AROUS           '),
(126, NULL, 3, 'ERRICALA            ', '2044', 'BEN AROUS           '),
(127, 7, 23, 'CITE MHIRI          ', '2045', 'TUNIS'),
(128, 7, 23, 'SIDI  DAOUD         ', '2046', 'TUNIS'),
(129, NULL, 3, 'HAMMAMLIF           ', '2050', 'BEN AROUS           '),
(130, 7, 23, 'EZZAHROUNI          ', '2051', 'TUNIS'),
(131, 7, 23, 'CITE EZZOUHOUR      ', '2052', 'TUNIS'),
(132, 7, 23, 'KABARIA             ', '2053', 'TUNIS'),
(133, NULL, 3, 'KHELIDIA            ', '2054', 'BEN AROUS           '),
(134, NULL, 3, 'BIR EL BEY          ', '2055', 'BEN AROUS           '),
(135, NULL, 1, 'RAOUED              ', '2056', 'ARIANA              '),
(136, NULL, 1, 'CHORFECH            ', '2057', 'ARIANA              '),
(137, NULL, 1, 'RIADH EL ANDALOUS   ', '2058', 'ARIANA              '),
(138, NULL, 3, 'BIR EL KASSAA       ', '2059', 'BEN AROUS           '),
(139, 7, 23, 'GOULETTE            ', '2060', 'TUNIS'),
(140, NULL, 1, 'PONT DE BIZERTE     ', '2061', 'ARIANA              '),
(141, 7, 23, 'CITE IBN KHALDOUN   ', '2062', 'TUNIS'),
(142, NULL, 3, 'NOUVELLE MEDINA     ', '2063', 'BEN AROUS           '),
(143, NULL, 3, 'JEBAL ERSAS         ', '2064', 'BEN AROUS           '),
(144, NULL, 3, 'EZZAHRA EL HABIB    ', '2065', 'BEN AROUS           '),
(145, 7, 23, 'IBN SINAA           ', '2066', 'TUNIS'),
(146, NULL, 13, 'CITE KHALED IBN OUALID   ', '2067', 'La Manouba             '),
(147, NULL, 3, 'EL MOUROUJ 3        ', '2068', 'BEN AROUS           '),
(148, NULL, 13, 'SIDI ALI HATTAB     ', '2071', 'La Manouba             '),
(149, 7, 23, 'CITE HELEL          ', '2072', 'TUNIS'),
(150, NULL, 1, 'BORJ LOUZIR         ', '2073', 'ARIANA              '),
(151, NULL, 3, 'EL MOUROUJ          ', '2074', 'BEN AROUS           '),
(152, NULL, 13, 'EL MANSOURA         ', '2075', 'La Manouba             '),
(153, 7, 23, 'LA MARSA ERRIADH    ', '2076', 'TUNIS'),
(154, 7, 23, 'MARSA SAF-SAF       ', '2078', 'TUNIS'),
(155, 7, 23, 'TUNIS  AERPORT      ', '2079', 'TUNIS'),
(156, NULL, 1, 'ARIANA              ', '2080', 'ARIANA              '),
(157, NULL, 1, 'BORJ TOUIL          ', '2081', 'ARIANA              '),
(158, NULL, 3, 'FOUCHANA            ', '2082', 'BEN AROUS           '),
(159, NULL, 1, 'CITE EL GHEZALA     ', '2083', 'ARIANA              '),
(160, NULL, 3, 'BORJ ESSEDRIA       ', '2084', 'BEN AROUS           '),
(161, 7, 23, 'CARTHAGE MOHAMED ALI', '2085', 'TUNIS'),
(162, NULL, 13, 'DOUAR HICHER        ', '2086', 'La Manouba             '),
(163, 7, 23, 'EL AGBA             ', '2087', 'TUNIS'),
(164, NULL, 1, 'PARC TECHNOLOGIQUE  ', '2088', 'ARIANA              '),
(165, 7, 23, 'LE KRAM OUEST       ', '2089', 'TUNIS'),
(166, NULL, 3, 'MORNAG              ', '2090', 'BEN AROUS           '),
(167, NULL, 1, 'EL MENZAH VI        ', '2091', 'ARIANA              '),
(168, 7, 23, 'EL MANAR 2          ', '2092', 'TUNIS'),
(169, NULL, 3, 'BOUKORNINE          ', '2093', 'BEN AROUS           '),
(170, NULL, 1, 'MNIHLA', '2094', 'ARIANA              '),
(171, NULL, 1, 'ETTADHAMEN 2        ', '2095', 'ARIANA              '),
(172, NULL, 3, 'EL YASMINET         ', '2096', 'BEN AROUS           '),
(173, NULL, 3, 'BOU MHEL            ', '2097', 'BEN AROUS           '),
(174, NULL, 3, 'RADES MEDINA        ', '2098', 'BEN AROUS           '),
(175, NULL, 3, 'CITE BOUSSOFFARA    ', '2099', 'BEN AROUS           '),
(176, NULL, 6, 'GAFSA               ', '2100', 'GAFSA               '),
(177, NULL, 6, 'MOULARES            ', '2110', 'GAFSA               '),
(178, NULL, 6, 'GAFSA GARE          ', '2111', 'GAFSA               '),
(179, NULL, 6, 'SIDI AHMED ZARROUG  ', '2112', 'GAFSA               '),
(180, NULL, 6, 'METLAOUI GARE       ', '2113', 'GAFSA               '),
(181, NULL, 6, 'BELKHIR             ', '2115', 'GAFSA               '),
(182, NULL, 6, 'ZANNOUCHE           ', '2116', 'GAFSA               '),
(183, NULL, 6, 'GAFSA ENTILAKA      ', '2117', 'GAFSA               '),
(184, NULL, 6, 'REDEYEF             ', '2120', 'GAFSA               '),
(185, NULL, 6, 'LALA                ', '2121', 'GAFSA               '),
(186, NULL, 6, 'ZOMRAT REDEYEF      ', '2122', 'GAFSA               '),
(187, NULL, 6, 'GAFSA CITE ENNOUR   ', '2123', 'GAFSA               '),
(188, NULL, 6, 'CITE ESSOUROUR      ', '2124', 'GAFSA               '),
(189, NULL, 6, 'BOU OMRANE           ', '2125', 'GAFSA               '),
(190, NULL, 6, 'METLAOUI            ', '2130', 'GAFSA               '),
(191, NULL, 6, 'SIDI AICH           ', '2131', 'GAFSA               '),
(192, NULL, 6, 'METLAOUI MINE       ', '2132', 'GAFSA               '),
(193, NULL, 6, 'GAFSA CITEDESJEUNES ', '2133', 'GAFSA               '),
(194, NULL, 6, 'METLAOUI THALJA     ', '2134', 'GAFSA               '),
(195, NULL, 6, 'HAOUEL EL OUED      ', '2135', 'GAFSA               '),
(196, NULL, 6, 'REDEYEFGARE         ', '2140', 'GAFSA               '),
(197, NULL, 6, 'MENZEL MIMOUN       ', '2141', 'GAFSA               '),
(198, NULL, 6, 'DOUALY GAFSA        ', '2143', 'GAFSA               '),
(199, NULL, 6, 'OULED BOUSSAD       ', '2145', 'GAFSA               '),
(200, NULL, 6, 'GAFSA AEROPORT      ', '2151', 'GAFSA               '),
(201, NULL, 6, 'MOULARES GARE       ', '2161', 'GAFSA               '),
(202, NULL, 6, 'ERRAGOUBA           ', '2169', 'GAFSA               '),
(203, NULL, 6, 'JEBEL MDILLA        ', '2170', 'GAFSA               '),
(204, NULL, 6, 'GFSA ENNAJEH', '2171', 'GAFSA               '),
(205, NULL, 6, 'BORJ EL MDHILA      ', '2173', 'GAFSA               '),
(206, NULL, 6, 'EL GUETTAR          ', '2180', 'GAFSA               '),
(207, NULL, 6, 'NICHIOU             ', '2181', 'GAFSA               '),
(208, NULL, 6, 'SENED               ', '2190', 'GAFSA               '),
(209, NULL, 6, 'MAJOURA             ', '2192', 'GAFSA               '),
(210, NULL, 6, 'ALIM                ', '2195', 'GAFSA               '),
(211, NULL, 6, 'ECHABIBA            ', '2196', 'GAFSA               '),
(212, NULL, 22, 'TOZEUR              ', '2200', 'TOZEUR              '),
(213, NULL, 22, 'TOZEUR CHOKRATSI    ', '2210', 'TOZEUR              '),
(214, NULL, 22, 'TAMERZA             ', '2212', 'TOZEUR              '),
(215, NULL, 22, 'TOZEUR AEROPORT     ', '2213', 'TOZEUR              '),
(216, NULL, 22, 'EL HAMMA DU DJERID  ', '2214', 'TOZEUR              '),
(217, NULL, 22, 'HEZOUA              ', '2223', 'TOZEUR              '),
(218, NULL, 22, 'EL MAHASSEN         ', '2224', 'TOZEUR              '),
(219, NULL, 22, 'BLED EL ADHAR       ', '2233', 'TOZEUR              '),
(220, NULL, 22, 'CHTAOUI SAHRAOUI    ', '2239', 'TOZEUR              '),
(221, NULL, 22, 'NEFTA               ', '2240', 'TOZEUR              '),
(222, NULL, 22, 'RAS DHRAA           ', '2241', 'TOZEUR              '),
(223, NULL, 22, 'CHORFA              ', '2243', 'TOZEUR              '),
(224, NULL, 22, 'BEN FARJALLAH       ', '2245', 'TOZEUR              '),
(225, NULL, 22, 'CHEBIKA DU JERID    ', '2253', 'TOZEUR              '),
(226, NULL, 22, 'DEGACHE             ', '2260', 'TOZEUR              '),
(227, NULL, 22, 'SABAA ABAR          ', '2261', 'TOZEUR              '),
(228, NULL, 22, 'BOUHLEL             ', '2263', 'TOZEUR              '),
(229, NULL, 17, 'SFAX                ', '3000', 'SFAX                '),
(230, NULL, 17, 'MAISON DES SERVICES SFAX', '3004', 'SFAX                '),
(231, NULL, 17, 'HENCHA              ', '3010', 'SFAX                '),
(232, NULL, 17, 'SAKIET EDDAIER      ', '3011', 'SFAX                '),
(233, NULL, 17, 'MERKEZ SAHNOUN      ', '3012', 'SFAX                '),
(234, NULL, 17, 'MERKEZ KASSAS       ', '3013', 'SFAX                '),
(235, NULL, 17, 'MELLITA             ', '3015', 'SFAX                '),
(236, NULL, 17, 'ELLOUZA             ', '3016', 'SFAX                '),
(237, NULL, 17, 'MENZEL HEDI CHAKER  ', '3020', 'SFAX                '),
(238, NULL, 17, 'SAKIET EZZIT        ', '3021', 'SFAX                '),
(239, NULL, 17, 'MERKEZ KAMOUN       ', '3022', 'SFAX                '),
(240, NULL, 17, 'OUED REMEL          ', '3023', 'SFAX                '),
(241, NULL, 17, 'HAZEG               ', '3026', 'SFAX                '),
(242, NULL, 17, 'SFAX EL JADIDA      ', '3027', 'SFAX                '),
(243, NULL, 17, 'AGAREB              ', '3030', 'SFAX                '),
(244, NULL, 17, 'MERKEZ BOU ACIDA      ', '3031', 'SFAX                '),
(245, NULL, 17, 'MERKEZ DEROUICHE    ', '3032', 'SFAX                '),
(246, NULL, 17, 'GRAIBA              ', '3034', 'SFAX                '),
(247, NULL, 17, 'ELATAYA             ', '3035', 'SFAX                '),
(248, NULL, 17, 'EL AMRA             ', '3036', 'SFAX                '),
(249, NULL, 17, 'CAID MHAMED         ', '3039', 'SFAX                '),
(250, NULL, 17, 'BIR ALI BEN KHALIFA ', '3040', 'SFAX                '),
(251, NULL, 17, 'MERKEZ CHIHYA       ', '3041', 'SFAX                '),
(252, NULL, 17, 'EL AIN              ', '3042', 'SFAX                '),
(253, NULL, 17, 'EL GRABA            ', '3043', 'SFAX                '),
(254, NULL, 17, 'NAKTA               ', '3044', 'SFAX                '),
(255, NULL, 17, 'BEB JEBLI           ', '3047', 'SFAX                '),
(256, NULL, 17, 'SOUK EL FERIANI     ', '3048', 'SFAX                '),
(257, NULL, 17, 'SFAX MAGHREB ARAB   ', '3049', 'SFAX                '),
(258, NULL, 17, 'CEKHIRA             ', '3050', 'SFAX                '),
(259, NULL, 17, 'MERKEZ ELALIA       ', '3051', 'SFAX                '),
(260, NULL, 17, 'CITE EL HABIB       ', '3052', 'SFAX                '),
(261, NULL, 17, 'MERKEZ SEBAI        ', '3054', 'SFAX                '),
(262, NULL, 17, 'OULED BOUSMIR       ', '3056', 'SFAX                '),
(263, NULL, 17, 'ELKHAZZNETTE        ', '3059', 'SFAX                '),
(264, NULL, 17, 'MAHARES             ', '3060', 'SFAX                '),
(265, NULL, 17, 'SIDI MANSOUR        ', '3061', 'SFAX                '),
(266, NULL, 17, 'SIDI ABBES          ', '3062', 'SFAX                '),
(267, NULL, 17, 'EL KHALIJ           ', '3063', 'SFAX                '),
(268, NULL, 17, 'CITE EL BAHRI       ', '3064', 'SFAX                '),
(269, NULL, 17, 'SFAX PORT           ', '3065', 'SFAX                '),
(270, NULL, 17, 'MERKEZ LAJMI        ', '3067', 'SFAX                '),
(271, NULL, 17, 'SFAX HACHED         ', '3069', 'SFAX                '),
(272, NULL, 17, 'KERKENNA            ', '3070', 'SFAX                '),
(273, NULL, 17, 'OUED CHAABOUNI      ', '3071', 'SFAX                '),
(274, NULL, 17, 'MERKEZ CHAKER       ', '3072', 'SFAX                '),
(275, NULL, 17, 'EL AOUABED          ', '3074', 'SFAX                '),
(276, NULL, 17, 'MERKEZ SGHAR        ', '3075', 'SFAX                '),
(277, NULL, 17, 'MERKEZ ALOUI        ', '3076', 'SFAX                '),
(278, NULL, 17, 'ELHAJEB             ', '3078', 'SFAX                '),
(279, NULL, 17, 'HAI EL KHIRI        ', '3079', 'SFAX                '),
(280, NULL, 17, 'JEBINIANA           ', '3080', 'SFAX                '),
(281, NULL, 17, 'ESSALTANIA          ', '3081', 'SFAX                '),
(282, NULL, 17, 'TYNA                ', '3083', 'SFAX                '),
(283, NULL, 17, 'TINA EL JADIDA      ', '3084', 'SFAX                '),
(284, NULL, 17, 'ENNIGROU            ', '3089', 'SFAX                '),
(285, NULL, 17, 'SIDI SALAH          ', '3091', 'SFAX                '),
(286, NULL, 17, 'MERKEZ OUALI        ', '3093', 'SFAX                '),
(287, NULL, 17, 'CITE BOURGUIBA      ', '3094', 'SFAX                '),
(288, NULL, 17, 'EL BOUSTEN          ', '3099', 'SFAX                '),
(289, NULL, 8, 'KAIROUAN            ', '3100', 'KAIROUAN            '),
(290, NULL, 8, 'SBIKHA              ', '3110', 'KAIROUAN            '),
(291, NULL, 8, 'BENSALEM            ', '3112', 'KAIROUAN            '),
(292, NULL, 8, 'AIN JELOULA         ', '3113', 'KAIROUAN            '),
(293, NULL, 8, 'MENZEL MHIRI        ', '3114', 'KAIROUAN            '),
(294, NULL, 8, 'SIDI SAAD           ', '3115', 'KAIROUAN            '),
(295, NULL, 8, 'CHERARDA            ', '3116', 'KAIROUAN            '),
(296, NULL, 8, 'OUSSELTIA           ', '3120', 'KAIROUAN            '),
(297, NULL, 8, 'CHEBIKA             ', '3121', 'KAIROUAN            '),
(298, NULL, 8, 'CITE EL HAJJEM      ', '3129', 'KAIROUAN            '),
(299, NULL, 8, 'HAFFOUZ             ', '3130', 'KAIROUAN            '),
(300, NULL, 8, 'KAIROUAN SUD        ', '3131', 'KAIROUAN            '),
(301, NULL, 8, 'SISSEB              ', '3132', 'KAIROUAN            '),
(302, NULL, 8, 'EL KARMA            ', '3133', 'KAIROUAN            '),
(303, NULL, 8, 'KAIROUAN OKBA       ', '3140', 'KAIROUAN            '),
(304, NULL, 8, 'EL BATEN            ', '3142', 'KAIROUAN            '),
(305, NULL, 8, 'EL ALA              ', '3150', 'KAIROUAN            '),
(306, NULL, 8, 'BIR AHMED           ', '3152', 'KAIROUAN            '),
(307, NULL, 8, 'HAJEB EL AYOUN      ', '3160', 'KAIROUAN            '),
(308, NULL, 8, 'NASRALLAH           ', '3170', 'KAIROUAN            '),
(309, NULL, 8, 'KAIROUAN MEDINA     ', '3172', 'KAIROUAN            '),
(310, NULL, 8, 'BOUHAJLA            ', '3180', 'KAIROUAN            '),
(311, NULL, 8, 'CITE ENNASR         ', '3182', 'KAIROUAN            '),
(312, NULL, 8, 'RAKKADA             ', '3191', 'KAIROUAN            '),
(313, NULL, 8, 'EL BORJ             ', '3198', 'KAIROUAN            '),
(314, NULL, 8, 'CITE IBN EL JAZZAR  ', '3199', 'KAIROUAN            '),
(315, NULL, 21, 'TATAOUINE           ', '3200', 'TATAOUINE           '),
(316, NULL, 21, 'BIR LAHMAR          ', '3212', 'TATAOUINE           '),
(317, NULL, 21, 'KSAR OUN            ', '3213', 'TATAOUINE           '),
(318, NULL, 21, 'GHOUMRASSEN         ', '3220', 'TATAOUINE           '),
(319, NULL, 21, 'KSAR MGABLA         ', '3221', 'TATAOUINE           '),
(320, NULL, 21, 'SMAR                ', '3223', 'TATAOUINE           '),
(321, NULL, 21, 'KIRCHAOU            ', '3225', 'TATAOUINE           '),
(322, NULL, 21, 'DOUIRET             ', '3232', 'TATAOUINE           '),
(323, NULL, 21, 'TATAOUINE EL MAHRAJENE', '3234', 'TATAOUINE           '),
(324, NULL, 21, 'REMADA              ', '3240', 'TATAOUINE           '),
(325, NULL, 21, 'EL FERCH            ', '3241', 'TATAOUINE           '),
(326, NULL, 21, 'KSAR DEBBEB          ', '3242', 'TATAOUINE           '),
(327, NULL, 21, 'OUED EL GHAR        ', '3243', 'TATAOUINE           '),
(328, NULL, 21, 'KSAR EL MOURABITINE ', '3251', 'TATAOUINE           '),
(329, NULL, 21, 'MAZTOURIA           ', '3252', 'TATAOUINE           '),
(330, NULL, 21, 'DEHIBAT             ', '3253', 'TATAOUINE           '),
(331, NULL, 21, 'KSAR EL HADADA      ', '3261', 'TATAOUINE           '),
(332, NULL, 21, 'BENI MEHIRA         ', '3262', 'TATAOUINE           '),
(333, NULL, 21, 'TATAOUINE ETTAHRIR', '3263', 'TATAOUINE           '),
(334, NULL, 21, 'GUERMASSA           ', '3271', 'TATAOUINE           '),
(335, NULL, 21, 'EZZAHRA TATAOUINE', '3272', 'TATAOUINE           '),
(336, NULL, 21, 'KSAR OULED SOLTAN   ', '3282', 'TATAOUINE           '),
(337, NULL, 21, 'BIR THLATHINE       ', '3284', 'TATAOUINE           '),
(338, NULL, 21, 'ROGBA               ', '3293', 'TATAOUINE           '),
(339, NULL, 20, 'SOUSSE              ', '4000', 'SOUSSE              '),
(340, NULL, 20, 'MAISON DES SERVICES SOUSSE', '4004', 'SOUSSE              '),
(341, NULL, 20, 'BOU FICHA           ', '4010', 'SOUSSE              '),
(342, NULL, 20, 'HAMMAM SOUSSE        ', '4011', 'SOUSSE              '),
(343, NULL, 20, 'HERGLA              ', '4012', 'SOUSSE              '),
(344, NULL, 20, 'MESSAIDINE          ', '4013', 'SOUSSE              '),
(345, NULL, 20, 'EL KANAISS          ', '4014', 'SOUSSE              '),
(346, NULL, 20, 'EL BORJINE          ', '4015', 'SOUSSE              '),
(347, NULL, 20, 'BENI KELTHOUM       ', '4016', 'SOUSSE              '),
(348, NULL, 20, 'HAMM SOUSSE GHARBIE ', '4017', 'SOUSSE              '),
(349, NULL, 20, 'KONDAR              ', '4020', 'SOUSSE              '),
(350, NULL, 20, 'KALAA ESSEGHIRA     ', '4021', 'SOUSSE              '),
(351, NULL, 20, 'AKOUDA              ', '4022', 'SOUSSE              '),
(352, NULL, 20, 'SOUSSE ERRIADH      ', '4023', 'SOUSSE              '),
(353, NULL, 20, 'MSAKEN HAI JEDID    ', '4024', 'SOUSSE              '),
(354, NULL, 20, 'SIDI EL HANI        ', '4025', 'SOUSSE              '),
(355, NULL, 20, 'SOUSSE BOUHSINA     ', '4027', 'SOUSSE              '),
(356, NULL, 20, 'ENFIDHA             ', '4030', 'SOUSSE              '),
(357, NULL, 20, 'SOUSSE EZZOUHOUR    ', '4031', 'SOUSSE              '),
(358, NULL, 20, 'MENZEL BELOUAER     ', '4032', 'SOUSSE              '),
(359, NULL, 20, 'MOUREDDINE          ', '4033', 'SOUSSE              '),
(360, NULL, 20, 'CHEGARNIA           ', '4034', 'SOUSSE              '),
(361, NULL, 20, 'AIN EL GARCI        ', '4035', 'SOUSSE              '),
(362, NULL, 20, 'SOUSSE SOUK LAHAD   ', '4037', 'SOUSSE              '),
(363, NULL, 20, 'SIDI BOU ALI        ', '4040', 'SOUSSE              '),
(364, NULL, 20, 'KSIBET SOUSSE       ', '4041', 'SOUSSE              '),
(365, NULL, 20, 'CHATT MERIAM        ', '4042', 'SOUSSE              '),
(366, NULL, 20, 'SOUSSE KHEZAMA      ', '4051', 'SOUSSE              '),
(367, NULL, 20, 'SAHLOUL             ', '4054', 'SOUSSE              '),
(368, NULL, 20, 'SOUSSE CORNICHE     ', '4059', 'SOUSSE              '),
(369, NULL, 20, 'KALAA KEBIRA        ', '4060', 'SOUSSE              '),
(370, NULL, 20, 'SOUSSE IBN KHALDOUN ', '4061', 'SOUSSE              '),
(371, NULL, 20, 'KALAA KEBIRA  KSAAR ', '4062', 'SOUSSE              '),
(372, NULL, 20, 'MSAKEN              ', '4070', 'SOUSSE              '),
(373, NULL, 20, 'KHEZAMA OUEST       ', '4071', 'SOUSSE              '),
(374, NULL, 20, 'ENFIDHA AEROPORT', '4080', 'SOUSSE              '),
(375, NULL, 20, 'ZAOUIET SOUSSE      ', '4081', 'SOUSSE              '),
(376, NULL, 20, 'HAMMAM SOUSSE PLAGE     ', '4083', 'SOUSSE              '),
(377, NULL, 20, 'EL KANTAOUI         ', '4089', 'SOUSSE              '),
(378, NULL, 20, 'AIN ERRAHMA         ', '4092', 'SOUSSE              '),
(379, NULL, 20, 'MSAKEN EL KEBLIA    ', '4099', 'SOUSSE              '),
(380, NULL, 14, 'Medenine', '4100', 'Medenine'),
(381, NULL, 14, 'BENI KHEDACHE       ', '4110', 'Medenine'),
(382, 1, 14, 'OUM ETTAMR          ', '4111', 'Medenine'),
(383, 1, 14, 'OUERJIJEN           ', '4112', 'Medenine'),
(384, NULL, 14, 'OULED AMOR          ', '4113', 'Medenine'),
(385, 1, 14, 'ERRAJA              ', '4114', 'Medenine'),
(386, 1, 14, 'MELLITA JERBA       ', '4115', 'Medenine'),
(387, NULL, 14, 'MIDOUN              ', '4116', 'Medenine'),
(388, 1, 14, 'JORF                ', '4117', 'Medenine'),
(389, NULL, 14, 'JERBA AEROPORT      ', '4120', 'Medenine'),
(390, NULL, 14, 'KOUTINE             ', '4121', 'Medenine'),
(391, NULL, 14, 'EL HICHEM              ', '4124', 'Medenine'),
(392, NULL, 14, 'EL GROO             ', '4125', 'Medenine'),
(393, NULL, 14, 'BENI MAAGUEL        ', '4126', 'Medenine'),
(394, NULL, 14, 'M_denine PERSEVER   ', '4127', 'Medenine'),
(395, NULL, 14, 'NOUVELLE M_denine   ', '4130', 'Medenine'),
(396, NULL, 14, 'HASSI AMOR          ', '4131', 'Medenine'),
(397, NULL, 14, 'HALG JEMAL          ', '4132', 'Medenine'),
(398, NULL, 14, 'ROBBANA             ', '4133', 'Medenine'),
(399, NULL, 14, 'CHAMAKH             ', '4134', 'Medenine'),
(400, NULL, 14, 'AJIM                ', '4135', 'Medenine'),
(401, NULL, 14, 'SEDGHIANE           ', '4136', 'Medenine'),
(402, NULL, 14, 'ZONE FRANCHE ZARZIS ', '4137', 'Medenine'),
(403, NULL, 14, 'BOUGHRARA           ', '4141', 'Medenine'),
(404, NULL, 14, 'M_denine EL MAARIFA ', '4142', 'Medenine'),
(405, NULL, 14, 'EL MOUENSA          ', '4144', 'Medenine'),
(406, NULL, 14, 'CEDOUIKECH          ', '4145', 'Medenine'),
(407, NULL, 14, 'ERRIADH             ', '4146', 'Medenine'),
(408, NULL, 14, 'METHANIA            ', '4150', 'Medenine'),
(409, NULL, 14, 'KSAR DJEDID         ', '4151', 'Medenine'),
(410, NULL, 14, 'RAS JEDIR           ', '4153', 'Medenine'),
(411, NULL, 14, 'EL GHRABATTE        ', '4154', 'Medenine'),
(412, NULL, 14, 'GUELLALA            ', '4155', 'Medenine'),
(413, NULL, 14, 'GHIZEN              ', '4156', 'Medenine'),
(414, NULL, 14, 'CHOUAMMAKH          ', '4159', 'Medenine'),
(415, NULL, 14, 'BEN GERDANE         ', '4160', 'Medenine'),
(416, NULL, 14, 'CHAHBANIA           ', '4163', 'Medenine'),
(417, NULL, 14, 'GRIBIS              ', '4164', 'Medenine'),
(418, NULL, 14, 'MAHBOUBINE          ', '4165', 'Medenine'),
(419, NULL, 14, 'OUED ZEBIB          ', '4166', 'Medenine'),
(420, NULL, 14, 'M_denine AL CHABEB ', '4167', 'Medenine'),
(421, NULL, 14, 'ZARZIS              ', '4170', 'Medenine'),
(422, NULL, 14, 'BEN GERDANE II      ', '4171', 'Medenine'),
(423, NULL, 14, 'SOUIHEL             ', '4173', 'Medenine'),
(424, NULL, 14, 'HASSI JERBI         ', '4174', 'Medenine'),
(425, NULL, 14, 'EL MAY              ', '4175', 'Medenine'),
(426, NULL, 14, 'ARKOU               ', '4176', 'Medenine'),
(427, NULL, 14, 'SIDI MEHREZ         ', '4179', 'Medenine'),
(428, NULL, 14, 'JERBA               ', '4180', 'Medenine'),
(429, NULL, 14, 'Srandi              ', '4182', 'Medenine'),
(430, NULL, 14, 'EL OUERSANIA        ', '4183', 'Medenine'),
(431, NULL, 14, 'SOUANI              ', '4185', 'Medenine'),
(432, NULL, 14, 'MEZRAYA             ', '4186', 'Medenine'),
(433, NULL, 14, 'SIDI ZAIED          ', '4190', 'Medenine'),
(434, NULL, 14, 'SIDI MAKHLOUF       ', '4191', 'Medenine'),
(435, NULL, 14, 'JALEL               ', '4192', 'Medenine'),
(436, NULL, 14, 'JMILA               ', '4193', 'Medenine'),
(437, NULL, 14, 'HMADI EL GUEBLI     ', '4194', 'Medenine'),
(438, NULL, 14, 'EL GACHIINE         ', '4195', 'Medenine'),
(439, NULL, 14, 'DAR JERBA           ', '4199', 'Medenine'),
(440, NULL, 10, 'K_bili              ', '4200', 'KEBILI              '),
(441, NULL, 10, 'REGIME MAATOUG      ', '4210', 'KEBILI              '),
(442, NULL, 10, 'RAHMET              ', '4211', 'KEBILI              '),
(443, NULL, 10, 'OUM SOMAA           ', '4212', 'KEBILI              '),
(444, NULL, 10, 'ZAOUIET ELANES      ', '4213', 'KEBILI              '),
(445, NULL, 10, 'JEMNA               ', '4214', 'KEBILI              '),
(446, NULL, 10, 'DOUZ CHARGUI        ', '4215', 'KEBILI              '),
(447, NULL, 10, 'DOUZ AOUINA         ', '4216', 'KEBILI              '),
(448, NULL, 10, 'NOUAIEL             ', '4222', 'KEBILI              '),
(449, NULL, 10, 'FATNASSA            ', '4223', 'KEBILI              '),
(450, NULL, 10, 'BAZMA               ', '4224', 'KEBILI              '),
(451, NULL, 10, 'SOUK EL AHAD        ', '4230', 'KEBILI              '),
(452, NULL, 10, 'BECHRI              ', '4231', 'KEBILI              '),
(453, NULL, 10, 'TENBIB              ', '4232', 'KEBILI              '),
(454, NULL, 10, 'RABTA               ', '4233', 'KEBILI              '),
(455, NULL, 10, 'EL GOLAA            ', '4234', 'KEBILI              '),
(456, NULL, 10, 'TOMBAR              ', '4235', 'KEBILI              '),
(457, NULL, 10, 'BOUABDALLAH         ', '4236', 'KEBILI              '),
(458, NULL, 10, 'TELMINE             ', '4237', 'KEBILI              '),
(459, NULL, 10, 'JANOURA             ', '4242', 'KEBILI              '),
(460, NULL, 10, 'BLIDETTE            ', '4243', 'KEBILI              '),
(461, NULL, 10, 'BECHELLI            ', '4253', 'KEBILI              '),
(462, NULL, 10, 'DOUZ                ', '4260', 'KEBILI              '),
(463, NULL, 10, 'ZAAFRANE            ', '4261', 'KEBILI              '),
(464, NULL, 10, 'JERSINE             ', '4263', 'KEBILI              '),
(465, NULL, 10, 'EL FAOUAR           ', '4264', 'KEBILI              '),
(466, NULL, 10, 'STAFTIMIA           ', '4273', 'KEBILI              '),
(467, NULL, 10, 'LIMAGUES            ', '4274', 'KEBILI              '),
(468, NULL, 10, 'KEBILI BEYEZ        ', '4280', 'KEBILI              '),
(469, NULL, 10, 'NOGGA               ', '4283', 'KEBILI              '),
(470, NULL, 10, 'MANSOURA KEBILI     ', '4293', 'KEBILI              '),
(471, NULL, 15, 'MONASTIR            ', '5000', 'MONASTIR            '),
(472, NULL, 15, 'OUARDANINE          ', '5010', 'MONASTIR            '),
(473, NULL, 15, 'KHENIS              ', '5011', 'MONASTIR            '),
(474, NULL, 15, 'SAHLINE             ', '5012', 'MONASTIR            '),
(475, NULL, 15, 'MENZEL KAMEL        ', '5013', 'MONASTIR            '),
(476, NULL, 15, 'BENI HASSEN         ', '5014', 'MONASTIR            '),
(477, NULL, 15, 'BOUHJAR             ', '5015', 'MONASTIR            '),
(478, NULL, 15, 'KSAR HELAL RIADH    ', '5016', 'MONASTIR            '),
(479, NULL, 15, 'ELHEDADRA           ', '5017', 'MONASTIR            '),
(480, NULL, 15, 'JEMMAL              ', '5020', 'MONASTIR            '),
(481, NULL, 15, 'BEMBLA              ', '5021', 'MONASTIR            '),
(482, NULL, 15, 'MENZEL ENNOUR       ', '5022', 'MONASTIR            '),
(483, NULL, 15, 'TOUZA               ', '5023', 'MONASTIR            '),
(484, NULL, 15, 'MENZEL FERSI        ', '5024', 'MONASTIR            '),
(485, NULL, 15, 'BENANE              ', '5025', 'MONASTIR            '),
(486, NULL, 15, 'ZAOUIET KONTOCH     ', '5028', 'MONASTIR            '),
(487, NULL, 15, 'JEMMEL KHEIREDDINE  ', '5030', 'MONASTIR            '),
(488, NULL, 15, 'KSIBET ELMEDIOUNI   ', '5031', 'MONASTIR            '),
(489, NULL, 15, 'MAZDOUR             ', '5032', 'MONASTIR            '),
(490, NULL, 15, 'MENZEL HAYET        ', '5033', 'MONASTIR            '),
(491, NULL, 15, 'CHERAHIL            ', '5034', 'MONASTIR            '),
(492, NULL, 15, 'SAYADA              ', '5035', 'MONASTIR            '),
(493, NULL, 15, 'MENZEL HARB         ', '5036', 'MONASTIR            '),
(494, NULL, 15, 'ZERAMDINE           ', '5040', 'MONASTIR            '),
(495, NULL, 15, 'MENZEL EL KHIR         ', '5041', 'MONASTIR            '),
(496, NULL, 15, 'MESJED AISSA        ', '5042', 'MONASTIR            '),
(497, NULL, 15, 'SIDI BENNOUR        ', '5044', 'MONASTIR            '),
(498, NULL, 15, 'MZAOUGHA            ', '5045', 'MONASTIR            '),
(499, NULL, 15, 'MOKNINE             ', '5050', 'MONASTIR            '),
(500, NULL, 15, 'MOKNINE EL JADIDA   ', '5051', 'MONASTIR            '),
(501, NULL, 15, 'MONASTIR REPUBLIQUE ', '5060', 'MONASTIR            '),
(502, NULL, 15, 'SIDI AMEUR          ', '5061', 'MONASTIR            '),
(503, NULL, 15, 'BOUDHER             ', '5063', 'MONASTIR            '),
(504, NULL, 15, 'MONASTIR AEROPORT   ', '5065', 'MONASTIR            '),
(505, NULL, 15, 'SOUKRINE            ', '5066', 'MONASTIR            '),
(506, NULL, 15, 'KSAR HELLAL         ', '5070', 'MONASTIR            '),
(507, NULL, 15, 'AMIRAT EL HOJJAJ       ', '5071', 'MONASTIR            '),
(508, NULL, 15, 'MENARA              ', '5076', 'MONASTIR            '),
(509, NULL, 15, 'MONASTIR GARE       ', '5079', 'MONASTIR            '),
(510, NULL, 15, 'TEBOULBA            ', '5080', 'MONASTIR            '),
(511, NULL, 15, 'CITE EL BASSATINE   ', '5089', 'MONASTIR            '),
(512, NULL, 15, 'BEKALTA             ', '5090', 'MONASTIR            '),
(513, NULL, 15, 'CHRAF               ', '5091', 'MONASTIR            '),
(514, NULL, 15, 'LAMTA               ', '5099', 'MONASTIR            '),
(515, 5, 12, 'MAHDIA              ', '5100', 'MAHDIA              '),
(516, NULL, 12, 'BOU MERDES          ', '5110', 'MAHDIA              '),
(517, NULL, 12, 'MAHDIA HIBOUN       ', '5111', 'MAHDIA              '),
(518, NULL, 12, 'KERKER              ', '5112', 'MAHDIA              '),
(519, NULL, 12, 'HBIRA               ', '5113', 'MAHDIA              '),
(520, NULL, 12, 'MELLOULECHE         ', '5114', 'MAHDIA              '),
(521, NULL, 12, 'BRADAA              ', '5115', 'MAHDIA              '),
(522, NULL, 12, 'SIDI ASSAKER        ', '5116', 'MAHDIA              '),
(523, NULL, 12, 'OULED CHAMAKH       ', '5120', 'MAHDIA              '),
(524, NULL, 12, 'REJICHE             ', '5121', 'MAHDIA              '),
(525, NULL, 12, 'CHEHIMET            ', '5123', 'MAHDIA              '),
(526, NULL, 12, 'TELALSA             ', '5124', 'MAHDIA              '),
(527, NULL, 12, 'SALAKTA             ', '5126', 'MAHDIA              '),
(528, NULL, 12, 'ESSAED              ', '5127', 'MAHDIA              '),
(529, NULL, 12, 'MAHDIA EL SOUK      ', '5129', 'MAHDIA              '),
(530, NULL, 12, 'CHOURBANE           ', '5130', 'MAHDIA              '),
(531, NULL, 12, 'EL HAKAIMA          ', '5131', 'MAHDIA              '),
(532, NULL, 12, 'NEFFATIA            ', '5135', 'MAHDIA              '),
(533, NULL, 12, 'EL GHEDHABNA        ', '5136', 'MAHDIA              '),
(534, NULL, 12, 'EL JEM ESSOUK       ', '5137', 'MAHDIA              '),
(535, NULL, 12, 'SOUASSI   ', '5140', 'MAHDIA              '),
(536, NULL, 12, 'CHIBA               ', '5141', 'MAHDIA              '),
(537, NULL, 12, 'MANSOURA SOUASSI    ', '5145', 'MAHDIA              '),
(538, NULL, 12, 'RECHARCHA           ', '5146', 'MAHDIA              '),
(539, NULL, 12, 'MAHDIA REPUBLIQUE   ', '5150', 'MAHDIA              '),
(540, NULL, 12, 'ZORDA               ', '5151', 'MAHDIA              '),
(541, NULL, 12, 'EL JEM              ', '5160', 'MAHDIA              '),
(542, NULL, 12, 'CHEBBA              ', '5170', 'MAHDIA              '),
(543, NULL, 12, 'KSOUR ESSAF         ', '5180', 'MAHDIA              '),
(544, NULL, 12, 'KSOUR ESSAF HACHED  ', '5189', 'MAHDIA              '),
(545, NULL, 12, 'SIDI ALOUANE        ', '5190', 'MAHDIA              '),
(546, NULL, 12, 'MAHDIA EZZAHRA      ', '5199', 'MAHDIA              '),
(547, NULL, 5, 'Gabes', '6000', 'Gabes'),
(548, NULL, 5, 'Gab?s HACHED        ', '6001', 'Gabes'),
(549, NULL, 5, 'METOUIA             ', '6010', 'Gabes'),
(550, NULL, 5, 'Gab?s EL MANARA     ', '6011', 'Gabes'),
(551, NULL, 5, 'SIDI BOULBABA       ', '6012', 'Gabes'),
(552, NULL, 5, 'SOMBAT              ', '6013', 'Gabes'),
(553, NULL, 5, 'M TORRECH           ', '6014', 'Gabes'),
(554, NULL, 5, 'AYOUN EZZERKINE     ', '6015', 'Gabes'),
(555, NULL, 5, 'ARRAM               ', '6016', 'Gabes'),
(556, NULL, 5, 'EL HAMMA            ', '6020', 'Gabes'),
(557, NULL, 5, 'GHANNOUCHE          ', '6021', 'Gabes'),
(558, NULL, 5, 'EL MDOU             ', '6022', 'Gabes'),
(559, NULL, 5, 'EL ALAYA            ', '6023', 'Gabes'),
(560, NULL, 5, 'NOUVELLE ZRAOUA     ', '6024', 'Gabes'),
(561, NULL, 5, 'ZARATH              ', '6026', 'Gabes'),
(562, NULL, 5, 'BOUATTOUCH          ', '6027', 'Gabes'),
(563, NULL, 5, 'MENZEL EL HABIB     ', '6030', 'Gabes'),
(564, NULL, 5, 'BOUCHEMMA           ', '6031', 'Gabes'),
(565, NULL, 5, 'TEBOULBOU           ', '6032', 'Gabes'),
(566, NULL, 5, 'CITE EL AMEL        ', '6033', 'Gabes'),
(567, NULL, 5, 'KETTANA             ', '6036', 'Gabes'),
(568, NULL, 5, 'Gab?s REPUBLIQUE    ', '6040', 'Gabes'),
(569, NULL, 5, 'CHENINI DE Gab?s    ', '6041', 'Gabes'),
(570, NULL, 5, 'EL AKARIT           ', '6042', 'Gabes'),
(571, NULL, 5, 'NOUVELLE MATMATA    ', '6044', 'Gabes'),
(572, NULL, 5, 'SIDI TOUATI         ', '6046', 'Gabes'),
(573, NULL, 5, 'NAHAL               ', '6051', 'Gabes'),
(574, NULL, 5, 'OUDHREF             ', '6052', 'Gabes'),
(575, NULL, 5, 'TAMEZRET            ', '6054', 'Gabes'),
(576, NULL, 5, 'DEKHILET TOUJANE    ', '6055', 'Gabes'),
(577, NULL, 5, 'EZZERKINE           ', '6056', 'Gabes'),
(578, NULL, 5, 'ELHAMMA SUD         ', '6060', 'Gabes'),
(579, NULL, 5, 'CHAT ESSALAM        ', '6061', 'Gabes'),
(580, NULL, 5, 'BEN GHILOUF         ', '6062', 'Gabes'),
(581, NULL, 5, 'MATMATA             ', '6070', 'Gabes'),
(582, NULL, 5, 'Gab?s PORT          ', '6071', 'Gabes'),
(583, NULL, 5, 'ZRIG                ', '6072', 'Gabes'),
(584, NULL, 5, 'MARETH              ', '6080', 'Gabes'),
(585, NULL, 5, 'METHOUIA EL MAYA    ', '6089', 'Gabes'),
(586, NULL, 5, 'SOUK EL MENZEL', '6091', 'Gabes'),
(587, NULL, 5, 'Gab?s EL HIDAYA     ', '6099', 'Gabes'),
(588, NULL, 19, 'SILIANA             ', '6100', 'SILIANA             '),
(589, NULL, 19, 'GAFOUR              ', '6110', 'SILIANA             '),
(590, NULL, 19, 'EL AKHOUAT GARE     ', '6111', 'SILIANA             '),
(591, NULL, 19, 'LE KRIB GARE        ', '6112', 'SILIANA             '),
(592, NULL, 19, 'BOU ROUIS           ', '6113', 'SILIANA             '),
(593, NULL, 19, 'KESRA               ', '6114', 'SILIANA             '),
(594, NULL, 19, 'EL AROUSSA          ', '6116', 'SILIANA             '),
(595, NULL, 19, 'KRIB                ', '6120', 'SILIANA             '),
(596, NULL, 19, 'EL KANTARA          ', '6123', 'SILIANA             '),
(597, NULL, 19, 'CITE ESSALAH        ', '6130', 'SILIANA             '),
(598, NULL, 19, 'EL MANSOURA         ', '6131', 'SILIANA             '),
(599, NULL, 19, 'BOUJLIDA            ', '6135', 'SILIANA             '),
(600, NULL, 19, 'MAKTAR              ', '6140', 'SILIANA             '),
(601, NULL, 19, 'KESRA SUPERIEURE    ', '6141', 'SILIANA             '),
(602, NULL, 19, 'ROHIA               ', '6150', 'SILIANA             '),
(603, NULL, 19, 'BARGOU              ', '6170', 'SILIANA             '),
(604, NULL, 19, 'BOU ARADA           ', '6180', 'SILIANA             '),
(605, NULL, 4, 'BIZERTE             ', '7000', 'BIZERTE             '),
(606, NULL, 4, 'SEJENANE            ', '7010', 'BIZERTE             '),
(607, NULL, 4, 'PECHERIE            ', '7011', 'BIZERTE             '),
(608, NULL, 4, 'BEZINA              ', '7012', 'BIZERTE             '),
(609, NULL, 4, 'EL AOUSJA           ', '7014', 'BIZERTE             '),
(610, NULL, 4, 'RAFRAF              ', '7015', 'BIZERTE             '),
(611, NULL, 4, 'EL ALIA             ', '7016', 'BIZERTE             '),
(612, NULL, 4, 'ZONE FRANCHE BIZERTE', '7017', 'BIZERTE             '),
(613, NULL, 4, 'JOUMINE             ', '7020', 'BIZERTE             '),
(614, NULL, 4, 'JARZOUNA            ', '7021', 'BIZERTE             '),
(615, NULL, 4, 'ZOUAOUINE           ', '7024', 'BIZERTE             '),
(616, NULL, 4, 'SOUNINE             ', '7025', 'BIZERTE             '),
(617, NULL, 4, 'EL AZIB             ', '7026', 'BIZERTE             '),
(618, NULL, 4, 'MATEUR              ', '7030', 'BIZERTE             '),
(619, NULL, 4, 'TINJA               ', '7032', 'BIZERTE             '),
(620, NULL, 4, 'GHARELMELH          ', '7033', 'BIZERTE             '),
(621, NULL, 4, 'METLINE             ', '7034', 'BIZERTE             '),
(622, NULL, 4, 'MENZEL ABDERRAHMANE ', '7035', 'BIZERTE             '),
(623, NULL, 4, 'GHEZALA             ', '7040', 'BIZERTE             '),
(624, NULL, 4, 'RAF RAF PLAGE       ', '7045', 'BIZERTE             '),
(625, NULL, 4, 'MATEUR 2            ', '7047', 'BIZERTE             '),
(626, NULL, 4, 'MENZEL BOURGUIBA    ', '7050', 'BIZERTE             '),
(627, NULL, 4, 'BIZERTE BOUGUATFA   ', '7053', 'BIZERTE             '),
(628, NULL, 4, 'UTIQUE              ', '7060', 'BIZERTE             '),
(629, NULL, 4, 'BIZERTE BAB MATEUR  ', '7061', 'BIZERTE             '),
(630, NULL, 4, 'UTIQUE NOUVELLE     ', '7063', 'BIZERTE             '),
(631, NULL, 4, 'RAS DJEBEL          ', '7070', 'BIZERTE             '),
(632, NULL, 4, 'BIZERTE HACHED      ', '7071', 'BIZERTE             '),
(633, NULL, 4, 'MZL BOURGUIBA ENNAJAH ', '7072', 'BIZERTE             '),
(634, NULL, 4, 'BENI ATTA           ', '7075', 'BIZERTE             '),
(635, NULL, 4, 'MENZEL JEMIL        ', '7080', 'BIZERTE             '),
(636, NULL, 4, 'EL KHETMINE         ', '7081', 'BIZERTE             '),
(637, NULL, 4, 'SIDI ALI CHEBAB     ', '7093', 'BIZERTE             '),
(638, NULL, 4, 'BORJ CHALLOUF       ', '7094', 'BIZERTE             '),
(639, NULL, 11, 'Le kef                 ', '7100', 'Le kef                 '),
(640, NULL, 11, 'NEBEUR              ', '7110', 'Le kef                 '),
(641, NULL, 11, 'TOUIREF             ', '7112', 'Le kef                 '),
(642, NULL, 11, 'EL KALAA KHASBA     ', '7113', 'Le kef                 '),
(643, NULL, 11, 'JERISSA             ', '7114', 'Le kef                 '),
(644, NULL, 11, 'ZITOUNA             ', '7115', 'Le kef                 '),
(645, NULL, 11, 'Le kef OUEST           ', '7117', 'Le kef                 '),
(646, NULL, 11, 'ESSAKIA             ', '7120', 'Le kef                 '),
(647, NULL, 11, 'BORJ EL AIFA        ', '7122', 'Le kef                 '),
(648, NULL, 11, 'KALAAT SINAN                            ', '7130', 'Le kef                 '),
(649, NULL, 11, 'SIDI KHIAR          ', '7131', 'Le kef                 '),
(650, NULL, 11, 'TAJEROUINE          ', '7150', 'Le kef                 '),
(651, NULL, 11, 'MENZEL SALEM        ', '7151', 'Le kef                 '),
(652, NULL, 11, 'EL KOSSOUR          ', '7160', 'Le kef                 '),
(653, NULL, 11, 'EDDAHMANI           ', '7170', 'Le kef                 '),
(654, NULL, 11, 'SERS                ', '7180', 'Le kef                 '),
(655, NULL, 16, 'NABEUL              ', '8000', 'NABEUL              '),
(656, NULL, 16, 'MENZEL BOUZELFA     ', '8010', 'NABEUL              '),
(657, NULL, 16, 'DAR CHABANE EL FEHR ', '8011', 'NABEUL              '),
(658, NULL, 16, 'FONDOUK JEDID       ', '8012', 'NABEUL              '),
(659, NULL, 16, 'MAAMOURA            ', '8013', 'NABEUL              '),
(660, NULL, 16, 'MENZEL HORR         ', '8015', 'NABEUL              '),
(661, NULL, 16, 'SOLIMAN             ', '8020', 'NABEUL              '),
(662, NULL, 16, 'BENI KHALLED        ', '8021', 'NABEUL              '),
(663, NULL, 16, 'BELLI               ', '8022', 'NABEUL              '),
(664, NULL, 16, 'SOMAA               ', '8023', 'NABEUL              '),
(665, NULL, 16, 'TAZARKA             ', '8024', 'NABEUL              '),
(666, NULL, 16, 'HAMMAM GHEZAZ       ', '8025', 'NABEUL              '),
(667, NULL, 16, 'SAHEB JEBEL         ', '8026', 'NABEUL              '),
(668, NULL, 16, 'SOLIMAN II          ', '8027', 'NABEUL              '),
(669, NULL, 16, 'GROMBALIA           ', '8030', 'NABEUL              '),
(670, NULL, 16, 'BIR MROUA           ', '8031', 'NABEUL              '),
(671, NULL, 16, 'SIDI JEDIDI         ', '8032', 'NABEUL              '),
(672, NULL, 16, 'DIAR EL HOJJAJ      ', '8033', 'NABEUL              '),
(673, NULL, 16, 'AZMOUR              ', '8035', 'NABEUL              '),
(674, NULL, 16, 'BOU ARGOUB          ', '8040', 'NABEUL              '),
(675, NULL, 16, 'KORBOUS             ', '8041', 'NABEUL              '),
(676, NULL, 16, 'BIR BOUREGBA        ', '8042', 'NABEUL              '),
(677, NULL, 16, 'BOUJERIDA           ', '8043', 'NABEUL              '),
(678, NULL, 16, 'EL MIDA             ', '8044', 'NABEUL              '),
(679, NULL, 16, 'EL HAOUARIA         ', '8045', 'NABEUL              '),
(680, NULL, 16, 'ZAOUIET ELMAGAIEZ   ', '8046', 'NABEUL              '),
(681, NULL, 16, 'HAMMAMET            ', '8050', 'NABEUL              '),
(682, NULL, 16, 'NIANOU              ', '8052', 'NABEUL              '),
(683, NULL, 16, 'DAR ALLOUCHE        ', '8055', 'NABEUL              '),
(684, NULL, 16, 'MANARA EL HAMMAMET  ', '8056', 'NABEUL              '),
(685, NULL, 16, 'YASMINE EL  HAMMAMET', '8057', 'NABEUL              '),
(686, NULL, 16, 'MREZGA              ', '8058', 'NABEUL              '),
(687, NULL, 16, 'BENI KHIAR          ', '8060', 'NABEUL              '),
(688, NULL, 16, 'SIDI DHAHER         ', '8061', 'NABEUL              '),
(689, NULL, 16, 'NABEUL THAMEUR      ', '8062', 'NABEUL              '),
(690, NULL, 16, 'KELIBIA EST         ', '8069', 'NABEUL              '),
(691, NULL, 16, 'KORBA               ', '8070', 'NABEUL              '),
(692, NULL, 16, 'DARCHAABANE PLAGE   ', '8075', 'NABEUL              '),
(693, NULL, 16, 'KORBA HACHED        ', '8076', 'NABEUL              '),
(694, NULL, 16, 'MENZEL TEMIME       ', '8080', 'NABEUL              '),
(695, NULL, 16, 'KHANGUET HOJJAJ     ', '8082', 'NABEUL              '),
(696, NULL, 16, 'TURKI               ', '8084', 'NABEUL              '),
(697, NULL, 16, 'KELIBIA             ', '8090', 'NABEUL              '),
(698, NULL, 16, 'GROMBALIA EZZOUHOUR ', '8092', 'NABEUL              '),
(699, NULL, 16, 'ZAOUIET JEDIDI      ', '8099', 'NABEUL              '),
(700, NULL, 7, 'JENDOUBA            ', '8100', 'JENDOUBA            '),
(701, NULL, 7, 'TABARKA             ', '8110', 'JENDOUBA            '),
(702, NULL, 7, 'AIN ESSOBH          ', '8112', 'JENDOUBA            '),
(703, NULL, 7, 'BENMETIR            ', '8114', 'JENDOUBA            '),
(704, NULL, 7, 'OUED MELIZ          ', '8115', 'JENDOUBA            '),
(705, NULL, 7, 'BOUAOUENE           ', '8116', 'JENDOUBA            '),
(706, NULL, 7, 'BABOUCH             ', '8121', 'JENDOUBA            '),
(707, NULL, 7, 'ESSANABEL           ', '8122', 'JENDOUBA            '),
(708, NULL, 7, 'BALTA               ', '8126', 'JENDOUBA            '),
(709, NULL, 7, 'AIN DRAHAM          ', '8130', 'JENDOUBA            '),
(710, NULL, 7, 'HAMMAM BOURGUIBA    ', '8136', 'JENDOUBA            '),
(711, NULL, 7, 'FERNANA             ', '8140', 'JENDOUBA            '),
(712, NULL, 7, 'GHARDIMAOU          ', '8160', 'JENDOUBA            '),
(713, NULL, 7, 'OUERGUECH           ', '8161', 'JENDOUBA            '),
(714, NULL, 7, 'BOU SALEM           ', '8170', 'JENDOUBA            '),
(715, NULL, 7, 'ERROMANI            ', '8172', 'JENDOUBA            '),
(716, NULL, 7, 'TABARKA AEROPORT    ', '8181', 'JENDOUBA            '),
(717, NULL, 7, 'JENDOUBA NORD       ', '8189', 'JENDOUBA            '),
(718, NULL, 7, 'EL MORJEN           ', '8192', 'JENDOUBA            '),
(719, NULL, 7, 'CITE ETTATAOUAR      ', '8196', 'JENDOUBA            '),
(720, 10, 2, 'Beja', '9000', 'Beja'),
(721, 10, 2, 'NEFZA               ', '9010', 'Beja'),
(722, 10, 2, 'OUECHTATA           ', '9012', 'Beja'),
(723, 10, 2, 'OUED ZARGUA         ', '9013', 'Beja'),
(724, 10, 2, 'ESSLOUGUIA          ', '9014', 'Beja'),
(725, 10, 2, 'SIDI SMAIL          ', '9021', 'Beja'),
(726, 10, 2, 'THIBAR              ', '9022', 'Beja'),
(727, 10, 2, 'EL MAAGOULA         ', '9023', 'Beja'),
(728, 10, 2, 'SIDI FREJ           ', '9029', 'Beja'),
(729, 10, 2, 'ZAHRET MEDIEN       ', '9030', 'Beja'),
(730, 10, 2, 'EL MZARA            ', '9031', 'Beja'),
(731, 10, 2, 'DOUGGA              ', '9032', 'Beja'),
(732, 10, 2, 'VAGA', '9033', 'Beja'),
(733, 10, 2, 'SIDI MEDIEN         ', '9034', 'Beja'),
(734, 10, 2, 'TEBOURSOUK          ', '9040', 'Beja'),
(735, 10, 2, 'HAMMAM SAYALA       ', '9052', 'Beja'),
(736, 10, 2, 'TESTOUR             ', '9060', 'Beja'),
(737, 10, 2, 'ESSKHIRA            ', '9061', 'Beja'),
(738, 10, 2, 'MEJEZ EL BAB        ', '9070', 'Beja'),
(739, 10, 2, 'GRIAAT              ', '9071', 'Beja'),
(740, 10, 2, 'GOUBELLAT           ', '9080', 'Beja'),
(741, NULL, 18, 'SIDI BOUZID         ', '9100', 'SIDI BOUZID         '),
(742, NULL, 18, 'JILMA               ', '9110', 'SIDI BOUZID         '),
(743, NULL, 18, 'OUM EL ADHAM        ', '9111', 'SIDI BOUZID         '),
(744, NULL, 18, 'EL FAIEDH           ', '9112', 'SIDI BOUZID         '),
(745, NULL, 18, 'BIR EL HAFFEY       ', '9113', 'SIDI BOUZID         '),
(746, NULL, 18, 'MENZEL BOUZAIENE    ', '9114', 'SIDI BOUZID         '),
(747, NULL, 18, 'SAIDA               ', '9115', 'SIDI BOUZID         '),
(748, NULL, 18, 'SIDI BOUZID EL WOUROUD', '9117', 'SIDI BOUZID         ');
INSERT INTO `delegation` (`id`, `grossiste_id`, `region_id`, `name`, `code`, `status`) VALUES
(749, NULL, 18, 'BEN AOUN             ', '9120', 'SIDI BOUZID         '),
(750, NULL, 18, 'SOUK JEDID', '9121', 'SIDI BOUZID         '),
(751, NULL, 18, 'CEBALA              ', '9122', 'SIDI BOUZID         '),
(752, NULL, 18, 'CITE OULED BEL HEDI ', '9125', 'SIDI BOUZID         '),
(753, NULL, 18, 'EL HICHRIA          ', '9131', 'SIDI BOUZID         '),
(754, NULL, 18, 'CITE EL KOUAFEL     ', '9132', 'SIDI BOUZID         '),
(755, NULL, 18, 'MEKNASSY            ', '9140', 'SIDI BOUZID         '),
(756, NULL, 18, 'MEZZOUNA            ', '9150', 'SIDI BOUZID         '),
(757, NULL, 18, 'OULED MNASSER       ', '9169', 'SIDI BOUZID         '),
(758, NULL, 18, 'REGUEB              ', '9170', 'SIDI BOUZID         '),
(759, NULL, 18, 'LESSOUDA            ', '9171', 'SIDI BOUZID         '),
(760, NULL, 18, 'OULED HAFFOUZ       ', '9180', 'SIDI BOUZID         ');

-- --------------------------------------------------------

--
-- Table structure for table `pays`
--

CREATE TABLE `pays` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pays`
--

INSERT INTO `pays` (`id`, `nom`, `code`, `created_at`, `updated_at`, `is_active`) VALUES
(2, 'Tunisie', 'tn', '2020-04-02 00:10:04', NULL, 1),
(3, 'La Réunion', 're', '2020-04-02 00:12:06', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `produits`
--

CREATE TABLE `produits` (
  `id` int(11) NOT NULL,
  `categorie_id` int(11) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nom` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `prix` double NOT NULL,
  `disponible` tinyint(1) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `nb_achat` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `sous_categorie_id` int(11) DEFAULT NULL,
  `pays_id` int(11) DEFAULT NULL,
  `station_id` int(11) DEFAULT NULL,
  `quantite` int(11) DEFAULT NULL,
  `code` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `produits`
--

INSERT INTO `produits` (`id`, `categorie_id`, `image`, `nom`, `description`, `prix`, `disponible`, `is_active`, `nb_achat`, `created_at`, `updated_at`, `sous_categorie_id`, `pays_id`, `station_id`, `quantite`, `code`) VALUES
(37, 1, '58de154f89544.png', 'EVOL. FULLTECH FE 5W30', 'Lubrifiant de technologie de synthèse ELF très hautes performances destiné à la lubrification de moteurs Diesel de véhicules de tourisme. Ce lubrifiant est particulièrement adapté aux véhicules Renault récents équipés d’un filtre à particules..\r\n\r\nSatisfait aux conditions d’utilisation les plus difficiles (ville, route, autoroute). Convient à toutes les conduites, en particulier sportives et à haut régime et à toutes les saisons.', 100, 1, 1, NULL, '2017-03-31 11:37:34', '2017-03-31 19:05:51', NULL, NULL, NULL, NULL, NULL),
(38, 1, '58de18a46acfc.png', 'EVOL. 900 FT 5W40', 'Lubrifiant de technologie de synthèse ELF très hautes performances destiné à la lubrification des moteurs de véhicules de tourisme Essence et Diesel.', 100, 1, 1, NULL, '2017-03-31 11:51:48', '2017-03-31 14:14:02', NULL, NULL, NULL, NULL, NULL),
(39, 1, '58de1a162625a.png', 'EVOL. 900 NF 5W40', 'Lubrifiant très hautes performances de technologie de synthèse ELF destiné à la lubrification des moteurs Essence de véhicules de tourisme.', 100, 1, 1, NULL, '2017-03-31 11:57:58', '2017-03-31 14:13:09', NULL, NULL, NULL, NULL, NULL),
(40, 1, '58de1bf33567e.png', 'ELFMATIC G3', 'Recommended for all automatic transmissions and hydraulic systems when the manufacturer requires an Automatic Transmission Fluid (ATF) GM DEXRON III. Also recommended for couplers and convertors, power-assisted steering systems', 100, 1, 1, NULL, '2017-03-31 12:05:55', '2017-03-31 14:12:52', NULL, NULL, NULL, NULL, NULL),
(41, 1, '58de2e3a8d24d.png', 'EVOL. 700 TURBO D 10W40', 'Lubrifiant de semi-synthèse ELF très hautes performances, spécialement développé pour les moteurs Diesel et optimisé pour répondre aux exigences accrues des technologies injection directe les plus récentes.\r\n\r\nParfaitement adapté à tous les types de parcours (ville, route, autoroute) et aux conditions les plus extrêmes (hautes températures).', 100, 1, 0, NULL, '2017-03-31 13:23:54', '2017-03-31 14:12:31', NULL, NULL, NULL, NULL, NULL),
(42, 1, '58de3011501bd.png', 'EVOL. 700 ST 10W40', 'Lubrifiant semi-synthèse ELF très hautes performances, développé pour les moteurs Essence et Diesel et répondant aux exigences accrues des technologies injection directe.', 100, 1, 0, NULL, '2017-03-31 13:31:45', '2017-03-31 14:12:14', NULL, NULL, NULL, NULL, NULL),
(43, 1, '58de311e00000.png', 'EVOL. 700 ST 10W40', 'Lubrifiant semi-synthèse ELF très hautes performances, développé pour les moteurs Essence et Diesel et répondant aux exigences accrues des technologies injection directe.', 100, 1, 1, NULL, '2017-03-31 13:36:14', '2017-03-31 13:36:26', NULL, NULL, NULL, NULL, NULL),
(44, 1, '58de323c7270e.png', 'TRANSELF NFJ 75W80', 'TRANSELF NFJ 75W-80 est un lubrifiant extrême pression de technologie de synthèse. Il est adapté aux véhicules RENAULT équipés de boîtes de vitesses des famille J, TL4 et NDX.', 100, 1, 1, NULL, '2017-03-31 13:41:00', '2017-03-31 13:41:40', NULL, NULL, NULL, NULL, NULL),
(45, 1, '58de33efaba95.png', 'PRESTIGRADE TS 20W50', 'Multigrade oil for petrol and diesel engines, formulated from a number of additives of carefully selected \r\nperformance and quantity.', 100, 1, 1, NULL, '2017-03-31 13:48:15', '2017-03-31 13:48:28', NULL, NULL, NULL, NULL, NULL),
(46, 1, '58de39145b8d8.png', 'PRESTIGRADE TS 15W40', 'Huile multigrade de conception moderne pour tous les moteurs de technologie récente, formulée à partir d’un ensemble d’additifs de performance soigneusement sélectionnés et dosés.', 100, 1, 1, NULL, '2017-03-31 14:10:12', '2017-03-31 14:11:49', NULL, NULL, NULL, NULL, NULL),
(47, 1, '58de3abeca2dd.png', 'PERF. SUPER D 15W40', '15W40 oil for trucks, buses and collecting waste vehicles (standard oil drain intervals) PERFORMANCE SUPER D 15W-40 retains excellent viscosity stability in service, ensuring efficient engine lubrication engine insevere conditions.\r\n\r\nDetergent, dispersant and anti-wear properties keep the engine clean enable efficient control of soot, sludge and piston deposits', 100, 1, 0, NULL, '2017-03-31 14:17:18', '2017-03-31 14:17:48', NULL, NULL, NULL, NULL, NULL),
(48, 1, '58de3b6840d99.png', 'PERFORMANCE SUPER D 40', 'Suitable for all turbocharged or normally aspirated Diesel engines of civil works machines, trucks, locomotives. \r\n\r\nSuitable for all Diesel stationary engines (energy production).\r\n\r\nAlso suitable for gear boxes, torque converters, hydraulic systems when the manufacturer requires an engine oil with an appropriate grade for these applications.', 100, 1, 0, NULL, '2017-03-31 14:20:08', '2017-03-31 14:20:46', NULL, NULL, NULL, NULL, NULL),
(49, 1, '58de783976417.png', 'TRANSELF EP 85W90', 'Transmission fluid designed for synchronized and non-synchronized gearboxes, mild loaded axles, transfer boxes and all gears requiring MIL-L-2105 or API GL-4 level. Approved by ZF for the lubrication of their gearboxes (without Intarder) with standard drain interval.', 100, 1, 1, NULL, '2017-03-31 18:39:37', '2017-03-31 18:46:32', NULL, NULL, NULL, NULL, NULL),
(50, 1, '58de798a4c4b4.png', 'ELFOLNA DS 46', 'High performance anti wear hydraulic oils for civil works and construction machineries', 100, 1, 0, NULL, '2017-03-31 18:45:14', '2017-03-31 18:45:14', NULL, NULL, NULL, NULL, NULL),
(51, 4, '5b6aca4c5902b-5e868c12c6a7d.jpeg', 'produit', 'dfsdg', 5, 0, 1, NULL, '2020-04-03 02:06:26', NULL, 2, 3, 1, 10, NULL),
(52, 4, '5b6aac96ec616-5e868d0bee53a.jpeg', 'produit', 'wfcwdv', 6, 1, 1, NULL, '2020-04-03 02:10:35', NULL, 2, 3, 1, 11, NULL),
(53, 3, '5df225c40a1c7-5e8692c88a7d9.jpeg', 'aa', 'bbhjh', 5, 1, 0, NULL, '2020-04-03 02:35:03', NULL, 1, 3, 1, 4, NULL),
(54, 3, NULL, 'aa', 'h,v,', 5, 1, 1, NULL, '2020-04-03 02:46:48', NULL, 1, 3, 1, 4, NULL),
(55, 3, NULL, 'aa', 'produittt', 5.5, 0, 1, NULL, '2020-04-03 02:47:09', NULL, 1, 3, 1, 0, NULL),
(56, 4, '5b6aac96ec616-5e869ab4009b4.jpeg', 'produit nqkjndqzk', 'kjnkln', 5.5, 1, 1, NULL, '2020-04-03 03:08:50', NULL, 2, 3, 1, 4, 0),
(57, 4, '5c5305bc05a8d-5e869bc432a61.jpeg', 'produit nqkjndqzk', 'vhh', 5.5, 1, 1, NULL, '2020-04-03 03:13:23', NULL, 2, 3, 1, 5, 2147483647),
(58, 4, NULL, 'frommage president 8pieces', 'nknk:n', 5.5, 1, 1, NULL, '2020-04-06 02:28:15', NULL, 2, 3, 1, 8, 500000000);

-- --------------------------------------------------------

--
-- Table structure for table `region`
--

CREATE TABLE `region` (
  `id` int(11) NOT NULL,
  `grossiste_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `region`
--

INSERT INTO `region` (`id`, `grossiste_id`, `name`, `status`) VALUES
(1, NULL, 'Ariana', ''),
(2, NULL, 'Béja', ''),
(3, NULL, 'Ben Arous', ''),
(4, NULL, 'Bizerte', ''),
(5, NULL, 'Gabès', ''),
(6, NULL, 'Gafsa', ''),
(7, NULL, 'Jendouba', ''),
(8, NULL, 'Kairouan', ''),
(9, NULL, 'Kasserine', ''),
(10, NULL, 'Kébili', ''),
(11, NULL, 'Le Kef', ''),
(12, NULL, 'Mahdia', ''),
(13, NULL, 'La Manouba', ''),
(14, NULL, 'Médenine', ''),
(15, NULL, 'Monastir', ''),
(16, NULL, 'Nabeul', ''),
(17, NULL, 'Sfax', ''),
(18, NULL, 'Sidi Bouzid', ''),
(19, NULL, 'Siliana', ''),
(20, NULL, 'Sousse', ''),
(21, NULL, 'Tataouine', ''),
(22, NULL, 'Touzeur', ''),
(23, NULL, 'Tunis', ''),
(24, NULL, 'Zaghouan', '');

-- --------------------------------------------------------

--
-- Table structure for table `sous_categories`
--

CREATE TABLE `sous_categories` (
  `id` int(11) NOT NULL,
  `pays_id` int(11) NOT NULL,
  `categorie_id` int(11) NOT NULL,
  `nom` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sous_categories`
--

INSERT INTO `sous_categories` (`id`, `pays_id`, `categorie_id`, `nom`, `created_at`, `updated_at`, `is_active`, `image`) VALUES
(1, 3, 3, 'Frommage', '2020-04-02 03:10:32', NULL, 1, NULL),
(2, 3, 4, 'Javel', '2020-04-02 03:26:15', NULL, 1, NULL),
(3, 3, 5, 'banane', '2020-04-07 12:45:14', NULL, 1, 'favicon2-5e8ce243ba694.png'),
(4, 3, 3, 'Lait', '2020-04-07 18:58:20', NULL, 1, 'bglogin1-5e8cbf3d84e40.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `station`
--

CREATE TABLE `station` (
  `id` int(11) NOT NULL,
  `pays_id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `adresse` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `zone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `heure_debut` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `heure_fin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `station`
--

INSERT INTO `station` (`id`, `pays_id`, `nom`, `adresse`, `created_at`, `updated_at`, `is_active`, `zone`, `heure_debut`, `heure_fin`) VALUES
(1, 3, 'Les Tamarins', 'La Saline Les Hauts ,Les Reunion', '2020-04-02 02:18:51', NULL, 1, 'SUD', NULL, NULL),
(2, 3, 'Le Butor', 'Le Port,Les Reunion', '2020-04-07 12:43:52', NULL, 1, 'SUD', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tva`
--

CREATE TABLE `tva` (
  `id` int(11) NOT NULL,
  `multiplicate` double NOT NULL,
  `nom` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `valeur` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tva`
--

INSERT INTO `tva` (`id`, `multiplicate`, `nom`, `valeur`) VALUES
(1, 0.982, 'TVA 1.75%', 1.75),
(2, 0.833, 'TVA 20%', 20);

-- --------------------------------------------------------

--
-- Table structure for table `unite`
--

CREATE TABLE `unite` (
  `id` int(11) NOT NULL,
  `libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `unite`
--

INSERT INTO `unite` (`id`, `libelle`) VALUES
(1, '1L'),
(2, '2L'),
(3, '4L'),
(4, '5L'),
(6, '8L');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `matricule_fiscale` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pays_id` int(11) DEFAULT NULL,
  `station_id` int(11) DEFAULT NULL,
  `tel` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `first_name`, `last_name`, `matricule_fiscale`, `pays_id`, `station_id`, `tel`) VALUES
(1, 'adnen', 'adnen.chouibi@gmail.com', 'adnen.chouibi@gmail.com', 'adnen.chouibi@gmail.com', 1, 'eokapbd63e8sc4g84s8w8k8wws4s80g', 'xe/v4ZHR5oa0uzhrhDDGMFbvsQC/JKCHoTQyMk4FMZj7qIthn1A+oQUh25yRwLnhuFmgw0PSl6VwXy8HdQXxGg==', '2017-01-03 12:21:49', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, '', '', '', NULL, NULL, NULL),
(2, 'anis', 'anis@gmail.com', 'anis@gmail.com', 'anis@gmail.com', 1, 'eokapbd63e8sc4g84s8w8k8wws4s80g', '3booCDELm5zhQ2Sq0WLcbVLETaZWBTnPkcpIjcb5/0kfAkyxFvqmrT/Rf9Y5hBLsjAzrhCC3JjrdPzZs96CZRA==', '2017-02-09 19:23:41', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Anis', 'Ben salah', NULL, NULL, NULL, NULL),
(3, 'gestionnaire', 'gestionnaire@total.tn', 'gestionnaire@total.tn', 'gestionnaire@total.tn', 1, '3h0bk5c8knmss0oss00w8o40gsw444c', 'DtL+wyAoSRV4OYPp8i5wW0WX8kzaLYhjX1wEdEBmeQK0wjRvrkr2L5yTktPBYROBI2Cf4QEdyDYIoM5kM94tdQ==', '2020-04-01 23:33:05', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:17:\"ROLE_GESTIONNAIRE\";}', 0, NULL, '', '', '', NULL, NULL, NULL),
(4, 'client', 'client', 'client@gmail.com', 'client@gmail.com', 1, 'pkcgr5454wg84cgsc0oocgk84c0wk8o', 'fxEkAeWBq6oIzg6bxgG8FK2dGokd7PNJzUzhICowEQekobHConxJx/GPWZsG+T62Vj7NZuvpB3OBPDTd0sMSAw==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, '', '', '', NULL, NULL, NULL),
(5, 'Grossiste', 'grossiste@gmail.com', 'grossiste@gmail.com', 'grossiste@gmail.com', 1, 'mri4b1le2yo0w4owos8cc0c88ssgkgg', 'QNa9wc71wzTugOtUZsTs1wgHVgK3MHqyy5BnamLGdO+7hu+X30xbn+TZ66RPX4avv8SMnelcu07Y9K8em+EHcw==', '2017-03-15 13:14:07', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:14:\"ROLE_GROSSISTE\";}', 0, NULL, NULL, 'Grossiste3', '', NULL, NULL, NULL),
(6, 'gest', 'gest@gmail.com', 'gest@gmail.com', 'gest@gmail.com', 1, '3jvkij3wzk6cog008wgwcw8kcgw8ok8', 'iMosXgcjvdTSIicCsFucakpjM+rvo4+EQkEzn6Z0nkIgr7sQYds79fieOS6uf7/gGQ8cbVnDPAZgbD9Qd15VTA==', NULL, 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:17:\"ROLE_GESTIONNAIRE\";}', 0, NULL, '', '', '', NULL, NULL, NULL),
(7, 'grossiste2@gmail.com', 'grossiste2@gmail.com', 'grossiste2@gmail.com', 'grossiste2@gmail.com', 1, 'dxt342qs6mg4ogcsc0c84gc0cs88k4w', 'lQqy7tMbidQ9l+wH4+KrRVKzz7sJ0tHAe88F+prJGoH0nKnFfUsMHpjW6jHQKWrF3xe3h6yQNiwQsqlTDk/0RA==', '2017-03-15 13:16:06', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:14:\"ROLE_GROSSISTE\";}', 0, NULL, NULL, 'Grossiste2', '', NULL, NULL, NULL),
(10, 'grossiste2', 'grossiste@grossiste.com', 'grossiste@grossiste.com', 'grossiste@grossiste.com', 1, '5pmq3u50330g4ck8g84s8c8s0sko0k8', '/XPHoM2XKmeQCcVYxmRW12fHJyWttpQzFYixsHtJooFZRw8kCckz7KTVBkVZCKTXH9vtFAqRcEsKaznmuTbmeQ==', '2017-01-02 23:40:05', 1, 0, NULL, NULL, NULL, 'a:1:{i:0;s:14:\"ROLE_GROSSISTE\";}', 0, NULL, NULL, 'Grossiste2', NULL, NULL, NULL, NULL),
(11, 'mourad', 'mourad', 'mourad@gmail.com', 'mourad@gmail.com', 1, 'eky4vs4s0pc8skwcogk4k8c0sk0scgc', 'qmLA6ra17ijHvCRd2hulzmsZhvmA7Zu6ZqFQzkXYuuunhKWFXXbVVl2jWheDv7q866Lgz/yjjsBtY/rs1f8XjA==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'L0464617', 'rania.ahmed@total.tn', 'rania.ahmed@total.tn', 'rania.ahmed@total.tn', 1, 'v2xirtgw41cs0cw08cowskgk8444g4', '6MgswJVsVUiRGZHyGQFHGBmQAUkhlKXzJJtoqJ390qBNoLH6C2yxpKTXgoIRgDaGCGSFnJYMmUd18WuoOW8Zqw==', '2017-06-12 15:05:05', 0, 0, NULL, 'ymoNHJW4GRZzZvyfBxUerygJkT0ArOvG896bt_ZR9nE', '2017-01-19 17:15:57', 'a:0:{}', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'adnen.singup@gmail.com', 'adnen.singup@gmail.com', 'adnen.singup@gmail.com', 'adnen.singup@gmail.com', 1, 'posr82om7eskkw8gg0wso4wwow84co4', '8tspfNQ/Ulgh/E/Vjait3Fd0JnRN7zLYY0lDz4jqr81nTHNmAEiNYY8Y3LwzSqzErZtF1IWjbA6ljoDZmQ8fhg==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'adnen', 'chouibi', '123456', NULL, NULL, NULL),
(14, 'adnen07@gmail.com', 'adnen07@gmail.com', 'adnen07@gmail.com', 'adnen07@gmail.com', 1, '1fbdd7icu728wc4wswko04cc8coc8ww', 'Krm5FYBLt5QyBVd5QcjQdNcpQWa5uc6ECaK5x5YfEJJHMHGA6zuB1Lf8PuTdyFPvpwH1AxMO41IPaLQMbkccfg==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Adnen', 'chouibi', '123456', NULL, NULL, NULL),
(15, 'adnen@gmail.com', 'adnen@gmail.com', 'adnen@gmail.com', 'adnen@gmail.com', 1, 'cu0l2jgz9i8ggggcc0w40wo0ogc8s4k', '0MFmf6JAm4blJIME/btPzc3k/xDCzvten3vBXamEefuJRoeiyME8iUDaRKg9r3dCO7Bl2SfnxJxhFWa+Il4d5g==', '2017-01-02 23:36:53', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Adnen', 'chouibi', '1221', NULL, NULL, NULL),
(16, 'sag@gnet.tn', 'sag@gnet.tn', 'sag@gnet.tn', 'sag@gnet.tn', 1, 'nsvg5xl3bsgssgkwooo0s0cgooc0wos', 'TkiAyRdoFYW4xMlM9dGAnZ14MHct771+MSg3JTDG2+BxM0e0i9YRAdF+s79CJTMzkoadUI4qb6JLwBrgCMvGRg==', '2017-01-11 12:11:08', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'FATHI', 'laarif', '20212X/A/M000', NULL, NULL, NULL),
(17, 'test@test.com', 'test@test.com', 'test@test.com', 'test@test.com', 1, 'hge4m3it4944sw80ccc0osgg0ks88kg', '9TIZJuTSeNQgZeB4yNMkxXpilkCNPQeECMP+9uC0fhiyIodrsq2zhoPLC2eEZUfryyjWwSWpsOymJ6D034U0jg==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'test', 'ben amor', NULL, NULL, NULL, NULL),
(18, 'adnen.signup@gmail.com', 'adnen.signup@gmail.com', 'adnen.signup@gmail.com', 'adnen.signup@gmail.com', 1, 's24zig0mz2o8ws4ogk4cw0scc4gg8gc', 'RWaCrKcIdLFrrPSvq5jr0bY7cyKSl0VCMVtkwj/59E3uhx2Xdi4G0BjgC0Prk0ulSu56NQZuFrO6nE76+Xn4Lw==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'adnen', 'chouibi', NULL, NULL, NULL, NULL),
(19, 'stagiaire.digital@total.tn', 'stagiaire.digital@total.tn', 'stagiaire.digital@total.tn', 'stagiaire.digital@total.tn', 1, 'ptpkistyxq8wgg800w8g44kk0c84s8c', '4qyNWs1LZOBXlPmMvpnblgledOhEjI0PhUn2ixkibgEr2vsFzmnmKwwVDBhn5yr1IanNjuSIqsG2UPyhZfEuwg==', '2017-06-13 12:53:07', 0, 0, NULL, 'HZ3YrBJBjQB7Y18IiaLB1hIXwtvwcKWJHM6VdHcyW_E', '2017-06-12 15:19:21', 'a:0:{}', 0, NULL, 'imen', 'makni', '0000', NULL, NULL, NULL),
(20, 'imenmakni5@gmail.com', 'imenmakni5@gmail.com', 'imenmakni5@gmail.com', 'imenmakni5@gmail.com', 0, '72ape4z6gkkk4o0oc88404wcgkskcsc', 'eao4k7vSTJeTqofBUrrhlYrIPYLuUZmBAgjOXD++KdsqrDtV3pdhiVKJ0/DqdVfsJlphIhOYXl9XUJ3m4gspEA==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'test', 'test', NULL, NULL, NULL, NULL),
(21, 'k.elhadjali@app4mob.net', 'k.elhadjali@app4mob.net', 'k.elhadjali@app4mob.net', 'k.elhadjali@app4mob.net', 1, '4vh8nbr2yosg0g4g44844ko4ko8s8wc', 'SFEgJ1YilBSsZSKt+AeLFerbe4hnzIilYJNdPw3y0cq0dS9BiiABMcVxK6i5MLEpsIgW91BdW1szGP9C4ImUBA==', '2020-03-27 08:26:37', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'EHA', 'Khadija', NULL, NULL, NULL, NULL),
(22, 'khadija.elhadjali@gmail.com', 'khadija.elhadjali@gmail.com', 'khadija.elhadjali@gmail.com', 'khadija.elhadjali@gmail.com', 1, '4vim58zunyg408o80okk8ks8g0wokgs', 'SCNNDo7Hctuum83ErTPMprcr/adNOCzKQiFcgGPA8573+rI9GlFBKj9veKz+KIHYG26ag96l7ydHMElkUeb1YA==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'EL HADJ ALI', 'Khadija', NULL, NULL, NULL, NULL),
(23, 'kh.elhadjali@gmail.com', 'kh.elhadjali@gmail.com', 'kh.elhadjali@gmail.com', 'kh.elhadjali@gmail.com', 1, 'pa329fl432o8gk84scs88c4k0gwwcwc', 'rHxZxeAZKekYsvE+AK668DND4q4h5EsYwFWgxtBL/RGjFXIIdb3yMz8jK09Vg0cUgcjPe+g0WZiMjCnFNNibVA==', '2018-11-15 12:43:25', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'EL HADJ ALI', 'Khadija', NULL, NULL, NULL, NULL),
(24, 'fatma.boussaid@gmail.com', 'fatma.boussaid@gmail.com', 'fatma.boussaid@gmail.com', 'fatma.boussaid@gmail.com', 1, '20khhvqxh1k0gwo4oscsgo4o0o444gs', 'j10scMcnE38UHD6LVF9xIqg+1ANLPpvTLCOUL1lXVLVJ4WiKTZF1149E8jyz9OtqrZD2obuKdjfp+8e3xN5eeA==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'boussaid', 'fatma', 'aze', NULL, NULL, NULL),
(25, 'suport@mytolcs.com', 'suport@mytolcs.com', 'suport@mytolcs.com', 'suport@mytolcs.com', 1, '9gv2zm1b49wk4s44gsoskcwkscs88og', '9VcHUp3tBgkEOAju88xlySTbFKz99LjiVLg45LHXK/g1zlxmL0CtjBiwrPsRJ0B90ZvWgAIr61DYuxI67Pc3hg==', NULL, 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', 0, NULL, 'adnen', 'chouibi', '123', 3, 1, NULL),
(27, 'admin@admin.com', 'admin@admin.com', 'admin@admin.com', 'admin@admin.com', 1, 'jointdk4wf4gkss48sosckcw48c44cc', 'kgRtyjhAcMd70QFhW6CBmNF2c8g7M2STwrMB2qqW62ToF+OifsMy05VIhj+Y6oc5OcXGup5g8kQuNvjlEkN7gA==', '2020-04-03 23:14:05', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}', 0, NULL, 'boussaid', 'fatma', NULL, 3, NULL, NULL),
(28, 'gestionnaire@gmail.com', 'gestionnaire@gmail.com', 'gestionnaire@gmail.com', 'gestionnaire@gmail.com', 1, 'tt16nahl1y8gssw0o4g4gcgs44sskss', 'RpUI3VvoPHuSPvPzwOq5NUFcSYDS50vMVKLEVQYihCQmLmuJSY2enTZQegTfreRKBAfYP3nZGRlABKiuHdk0OQ==', '2020-04-07 21:39:13', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:17:\"ROLE_GESTIONNAIRE\";}', 0, NULL, NULL, NULL, NULL, 3, 1, 58256487),
(30, 'gerant@gmail.com', 'gerant@gmail.com', 'gerant@gmail.com', 'gerant@gmail.com', 1, 'dmllpuhhc0ococgkkcw8ssggs0gkgw0', 'ttlurBiaqfRP6nXFArIvcuTNBl7k/puOXpHSexbZjskPERddD8xr9MP+3rvwqsSPzsLmHbj9/7yTbls/x2V4sA==', '2020-04-07 21:49:48', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:11:\"ROLE_GERANT\";}', 0, NULL, 'fatma', 'boussaid', NULL, 3, 1, NULL),
(31, 'fatma.boussaid1994@gmail.com', 'fatma.boussaid1994@gmail.com', 'fatma.boussaid1994@gmail.com', 'fatma.boussaid1994@gmail.com', 1, '26rxlvtwjl28c004ows4sssokcwsw4s', 'e93LjkGH8WNw5NWxvylhhS42E0fnL7LRftZf90vuhYjLAsg+2q089sZpokyML2g1qIOvkKb9xjCc5b++xXXe0w==', NULL, 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', 0, NULL, 'fatma', 'boussaid', NULL, 3, 1, 58256487),
(32, 'khadija@gmail.com', 'khadija@gmail.com', 'khadija@gmail.com', 'khadija@gmail.com', 1, '14ljmj93tuo0sooco804g40gsogws4g', 'IEiF9qG2UmG4LQJalbC6pNFhA/9NraeZ8MMAZM/QznoaL3cd33ubKw+GNPHKqu8sLhRqxfiyi5lGLg+3h3YIVw==', NULL, 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:11:\"ROLE_GERANT\";}', 0, NULL, 'khadija', 'khadija', NULL, 3, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `useradresses`
--

CREATE TABLE `useradresses` (
  `id` int(11) NOT NULL,
  `utilisateur_id` int(11) DEFAULT NULL,
  `nom` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `adresse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cp` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `pays` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `ville` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `complement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `useradresses`
--

INSERT INTO `useradresses` (`id`, `utilisateur_id`, `nom`, `prenom`, `telephone`, `adresse`, `cp`, `pays`, `ville`, `complement`) VALUES
(5, 1, 'Chouibi', 'Adnen', '52926065', 'Res mercure A3-5', '2094', '', 'Kasserine', NULL),
(6, 1, 'Chouibi', 'Adnen', '52926065', 'Res mercure A3-5', '1230', 'Tunisie', 'KASSERINE NOUR', NULL),
(8, 1, 'Anis', 'Ayachi', '52926065', '52 rue Besbes Morouj 2', '1100', 'Zaghouan', 'ZAGHOUAN', NULL),
(10, 1, 'ayachi', 'mounir', '96199089', '91 rue chebt', '2074', 'Ben Arous', 'EL MOUROUJ', NULL),
(11, 2, 'foulen', 'foulen', '20215114', 'addresse', '1001', 'Tunis', 'TUNIS REPUBLIQUE', NULL),
(12, 4, 'Mohamed', 'Saleh', '21521478', 'Tunis', '1001', 'Tunis', 'TUNIS REPUBLIQUE', NULL),
(15, 12, 'Ahmed', 'Rania', '54114823', 'rades', '2040', 'Ben Arous', 'RADES               ', NULL),
(20, 17, 'chouibi', 'adnen', '+216 96199089', '29 rue andirghandi', '1000', 'Tunis', 'TUNIS RECETTE PRINCIPALE', NULL),
(21, 3, 'test', 'test', '+216 26554556', 'megrine', '2033', 'Ben Arous', 'MEGRINE             ', NULL),
(22, 21, 'el hadj ali', 'khadija', '+216 22469495', '2 , rue Murabite El Manzeh 5, blob E', '2035', 'Ariana', 'TUNIS CARTHAGE      ', NULL),
(23, 21, 'el hadj ali', 'khadija', '+216 22469495', '2 , rue Murabite El Manzeh 5, blob E', '2035', 'Ariana', 'TUNIS CARTHAGE      ', NULL),
(24, 23, 'el hadj ali', 'khadija', '+216 22469495', '2 , rue Murabite El Manzeh 5, blob E', '2035', 'Ariana', 'TUNIS CARTHAGE      ', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_3AF34668A6E44244` (`pays_id`);

--
-- Indexes for table `commandes`
--
ALTER TABLE `commandes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_35D4282CFB88E14F` (`utilisateur_id`),
  ADD KEY `IDX_35D4282CA6E44244` (`pays_id`),
  ADD KEY `IDX_35D4282C21BDB235` (`station_id`);

--
-- Indexes for table `commande_produit`
--
ALTER TABLE `commande_produit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_DF1E9E8782EA2E54` (`commande_id`),
  ADD KEY `IDX_DF1E9E87F347EFB` (`produit_id`);

--
-- Indexes for table `delegation`
--
ALTER TABLE `delegation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_292F436D407EB501` (`grossiste_id`),
  ADD KEY `IDX_292F436D98260155` (`region_id`);

--
-- Indexes for table `pays`
--
ALTER TABLE `pays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produits`
--
ALTER TABLE `produits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_BE2DDF8CBCF5E72D` (`categorie_id`),
  ADD KEY `IDX_BE2DDF8C365BF48` (`sous_categorie_id`),
  ADD KEY `IDX_BE2DDF8CA6E44244` (`pays_id`),
  ADD KEY `IDX_BE2DDF8C21BDB235` (`station_id`);

--
-- Indexes for table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_8CEF440407EB501` (`grossiste_id`);

--
-- Indexes for table `sous_categories`
--
ALTER TABLE `sous_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_DC8B1382A6E44244` (`pays_id`),
  ADD KEY `IDX_DC8B1382BCF5E72D` (`categorie_id`);

--
-- Indexes for table `station`
--
ALTER TABLE `station`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9F39F8B1A6E44244` (`pays_id`);

--
-- Indexes for table `tva`
--
ALTER TABLE `tva`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unite`
--
ALTER TABLE `unite`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`),
  ADD KEY `UNIQ_8D93D649A6E44244` (`pays_id`) USING BTREE,
  ADD KEY `UNIQ_8D93D64921BDB235` (`station_id`) USING BTREE;

--
-- Indexes for table `useradresses`
--
ALTER TABLE `useradresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_C35CDA25FB88E14F` (`utilisateur_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `commandes`
--
ALTER TABLE `commandes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `commande_produit`
--
ALTER TABLE `commande_produit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `delegation`
--
ALTER TABLE `delegation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=761;
--
-- AUTO_INCREMENT for table `pays`
--
ALTER TABLE `pays`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `produits`
--
ALTER TABLE `produits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `region`
--
ALTER TABLE `region`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `sous_categories`
--
ALTER TABLE `sous_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `station`
--
ALTER TABLE `station`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tva`
--
ALTER TABLE `tva`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `unite`
--
ALTER TABLE `unite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `useradresses`
--
ALTER TABLE `useradresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `FK_3AF34668A6E44244` FOREIGN KEY (`pays_id`) REFERENCES `pays` (`id`);

--
-- Constraints for table `commandes`
--
ALTER TABLE `commandes`
  ADD CONSTRAINT `FK_35D4282C21BDB235` FOREIGN KEY (`station_id`) REFERENCES `station` (`id`),
  ADD CONSTRAINT `FK_35D4282CA6E44244` FOREIGN KEY (`pays_id`) REFERENCES `pays` (`id`),
  ADD CONSTRAINT `FK_35D4282CFB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `commande_produit`
--
ALTER TABLE `commande_produit`
  ADD CONSTRAINT `FK_DF1E9E8782EA2E54` FOREIGN KEY (`commande_id`) REFERENCES `commandes` (`id`),
  ADD CONSTRAINT `FK_DF1E9E87F347EFB` FOREIGN KEY (`produit_id`) REFERENCES `produits` (`id`);

--
-- Constraints for table `delegation`
--
ALTER TABLE `delegation`
  ADD CONSTRAINT `AAZZ` FOREIGN KEY (`grossiste_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `SDD` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`);

--
-- Constraints for table `produits`
--
ALTER TABLE `produits`
  ADD CONSTRAINT `FK_BE2DDF8C21BDB235` FOREIGN KEY (`station_id`) REFERENCES `station` (`id`),
  ADD CONSTRAINT `FK_BE2DDF8C365BF48` FOREIGN KEY (`sous_categorie_id`) REFERENCES `sous_categories` (`id`),
  ADD CONSTRAINT `FK_BE2DDF8CA6E44244` FOREIGN KEY (`pays_id`) REFERENCES `pays` (`id`),
  ADD CONSTRAINT `FK_BE2DDF8CBCF5E72D` FOREIGN KEY (`categorie_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `region`
--
ALTER TABLE `region`
  ADD CONSTRAINT `FK_22A089CC407EB501` FOREIGN KEY (`grossiste_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `sous_categories`
--
ALTER TABLE `sous_categories`
  ADD CONSTRAINT `FK_DC8B1382A6E44244` FOREIGN KEY (`pays_id`) REFERENCES `pays` (`id`),
  ADD CONSTRAINT `FK_DC8B1382BCF5E72D` FOREIGN KEY (`categorie_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `station`
--
ALTER TABLE `station`
  ADD CONSTRAINT `FK_9F39F8B1A6E44244` FOREIGN KEY (`pays_id`) REFERENCES `pays` (`id`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_8D93D64921BDB235` FOREIGN KEY (`station_id`) REFERENCES `station` (`id`),
  ADD CONSTRAINT `FK_8D93D649A6E44244` FOREIGN KEY (`pays_id`) REFERENCES `pays` (`id`);

--
-- Constraints for table `useradresses`
--
ALTER TABLE `useradresses`
  ADD CONSTRAINT `FK_C35CDA25FB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
